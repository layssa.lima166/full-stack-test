import { useContext, useState } from "react";
import { AuthContext } from "../../context/auth";
import { Formik, Form, Field } from "formik";
import { IAuthLogin } from "../../server";
import { ErrorToast } from "../../components/toasts";
import { useNavigate } from "react-router-dom";

export default function Login() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<undefined | string>();
  const navigate = useNavigate();

  const { handleLogin } = useContext(AuthContext);

  async function handleSubmit(values: IAuthLogin) {
    try {
      setLoading(true);
      const response = await handleLogin(values);

      localStorage.setItem("auth", response.data.token);
      localStorage.setItem("id", response.data.id);

      setLoading(false);
      navigate("/platform");
    } catch (error) {
      console.log(error);
      setLoading(false);

      const message = error.response.data;

      if (message == "incorrect_data") {
        setError("Senha ou email incorreto. Revise os dados.");
        ErrorToast("Email ou senha incorretos");
        return;
      }
      if (message == "user_not_found") {
        setError("Usuário não encontrado.");
        ErrorToast("Usuário não encontrado.");
        return;
      }
    }
  }

  function goToRegister() {
    navigate("/register");
  }

  return (
    <div className="w-full h-full bg-gray-900 flex flex-col gap-10 items-center py-10 px-5 md:px-10">
      <h3 className="text-2xl text-white font-semibold">Seja bem-vindo!</h3>
      <p className="text-xl text-white font-medium">
        {" "}
        Faça login na sua conta.
      </p>

      <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={handleSubmit}
        className="w-full"
      >
        <Form className="w-full flex flex-col gap-8 items-center">
          <div className="w-full text-white flex flex-col px-2 gap-2">
            <label className="font-semibold">Email</label>
            <Field
              name="email"
              type="email"
              className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-14 px-2 "
            />
          </div>

          <div className="w-full text-white flex flex-col px-2 gap-2">
            <label className="font-semibold">Senha</label>
            <Field
              name="password"
              type="password"
              className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-14 px-2"
            />
          </div>

          <button
            disabled={!!loading}
            type="submit"
            className="bg-primary w-full font-semibold h-14 font-semibold"
          >
            {loading ? "Entrando" : "Entrar"}
          </button>

          {error && <span className="text-error text-lg">{error}</span>}

          <p className="text-lg">
            Não tem conta?{" "}
            <span className="underline cursor-pointer" onClick={goToRegister}>
              Crie Agora
            </span>
          </p>
        </Form>
      </Formik>
    </div>
  );
}
