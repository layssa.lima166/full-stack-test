import { useContext, useState } from "react";
import { Formik, Form, Field } from "formik";
import { IAuthRegister } from "../../server";
import { ErrorToast, SuccessToast } from "../../components/toasts";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../context/auth";

export default function Register() {
  const [error, setError] = useState<undefined | string>();
  const { handleSignUp } = useContext(AuthContext);
  const navigate = useNavigate();

  async function handleSubmit(values: IAuthRegister) {
    try {
      if (values.password != values.repeatPassword) {
        setError("As senhas são diferentes");
        return;
      } else setError(undefined);

      const response = await handleSignUp(values);

      localStorage.setItem("auth", response.data.token);
      localStorage.setItem("id", response.data.id);

      SuccessToast("Cadastro realizado!");
      navigate("/platform");
    } catch (error) {
      console.log(error);

      ErrorToast("Não foi possível criar o usuário");
    }
  }

  function goToLogin() {
    navigate("/login");
  }

  return (
    <div className="w-full h-auto  bg-gray-900 flex flex-col gap-10 items-center py-10 px-5 md:px-10">
      <h3 className="text-2xl text-white font-semibold">Bem-vindo!</h3>
      <p className="text-xl text-white font-medium"> Crie sua conta.</p>

      <Formik
        initialValues={{
          email: "",
          password: "",
          repeatPassword: "",
          name: "",
        }}
        onSubmit={handleSubmit}
        className="w-full"
      >
        <Form className="w-full flex flex-col gap-8 items-center">
          <div className="w-full text-white flex flex-col px-2 gap-2">
            <label className="font-semibold" htmlFor="name">
              Name
            </label>
            <Field
              name="name"
              type="name"
              className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-14 px-2 "
            />
          </div>

          <div className="w-full text-white flex flex-col px-2 gap-2">
            <label className="font-semibold" htmlFor="email">
              Email
            </label>
            <Field
              name="email"
              type="email"
              className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-14 px-2 "
            />
          </div>

          <div className="w-full text-white flex flex-col px-2 gap-2">
            <label className="font-semibold" htmlFor="password">
              Senha
            </label>
            <Field
              name="password"
              type="password"
              className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-14 px-2"
            />
          </div>

          <div className="w-full text-white flex flex-col px-2 gap-2">
            <label className="font-semibold" htmlFor="repeatPassword">
              Repetir senha
            </label>
            <Field
              name="repeatPassword"
              type="password"
              className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-14 px-2"
            />
          </div>

          <button
            type="submit"
            className="bg-primary w-full font-semibold h-14 font-semibold"
          >
            Entrar
          </button>

          {error && <span className="text-error text-lg">{error}</span>}

          <p className="text-lg">
            Já tem conta?{" "}
            <span className="underline cursor-pointer" onClick={goToLogin}>
              Faça Login
            </span>
          </p>
        </Form>
      </Formik>
    </div>
  );
}
