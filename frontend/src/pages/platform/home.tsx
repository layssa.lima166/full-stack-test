import { useEffect, useState } from "react";
import {
  ICreateMovie,
  IEditMovie,
  addMovieToList,
  createMovie,
  deleteMovie,
  editMovie,
  getAllMovies,
} from "../../server";
import {
  ErrorToast,
  SuccessToast,
  WarmingToast,
} from "../../components/toasts";
import { IMovieCard, MovieCard } from "../../components/movieCard";
import { AddIcon } from "../../components/icons/add";
import { XIcon } from "../../components/icons/x";

import { Button } from "../../components/button";
import { Field, Form, Formik } from "formik";
import { getBase64 } from "../../utils/converteFile";
import { formateDate } from "../../utils/formatDate";
import { Pagination } from "../../components/Pagination";

export default function Home() {
  const [addMovieModal, setAddMovieModal] = useState(false);
  const [editMovie, setEditMovie] = useState<object | undefined>();
  const [toDelete, setToDelete] = useState<string | undefined>();

  const [loading, setLoading] = useState(true);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);

  const [movies, setMovies] = useState<IMovieCard[]>([]);

  function cancelDelete() {
    setToDelete(undefined);
  }

  async function confirmDelete() {
    try {
      if (toDelete) {
        await deleteMovie({ id: toDelete });
        SuccessToast("Filme removido!");
      }
      setToDelete(undefined);
      setCurrentPage(0);
      void getMovies();
    } catch (error) {
      console.log(error);
      ErrorToast("Não foi possível apagar o filme.");
    }
  }

  function chooseDelete(_id: string) {
    setToDelete(_id);
  }

  async function getMovies() {
    try {
      const response = await getAllMovies({
        pageSize: 12,
        pageOffset: currentPage,
      });

      if (response.data.data.length) {
        setMovies(response.data.data);
        const totalPages_ = Math.ceil(response.data.totalItems / 12);

        setTotalPages(totalPages_);
        setLoading(false);
      }
    } catch (error) {
      console.log(error);
      ErrorToast("Ocorreu um erro ao tentar carregar os filmes.");
      setLoading(false);
    }
  }

  return (
    <div className="w-full h-auto min-h-screen flex flex-col items-start">
      <MovieForm
        refresh={getMovies}
        openModal={addMovieModal}
        closeModal={() => setAddMovieModal(false)}
      />

      <MovieForm
        refresh={getMovies}
        openModal={Boolean(editMovie)}
        movie={editMovie}
        closeModal={() => setEditMovie(undefined)}
      />

      <RemoveMovieModal
        openModal={Boolean(toDelete)}
        confirmeDelete={confirmDelete}
        cancelDelete={cancelDelete}
      />

      <Banner openAddMovieModal={() => setAddMovieModal(true)} />

      <ListOfMovies
        editMovie={setEditMovie}
        deleteMovie={chooseDelete}
        getMovies={getMovies}
        currentPage={currentPage}
        loading={loading}
        totalPages={totalPages}
        movies={movies}
        handleChangeCurrentPage={setCurrentPage}
      />
    </div>
  );
}

function Banner({ openAddMovieModal }: { openAddMovieModal: () => void }) {
  return (
    <div className="px-4 flex flex-col gap-8 text-xl md:text-4xl text-gray-200  flex items-center  justify-center w-full h-[15vh] md:h-[30vh]  overflow-hidden bg-gradient-to-t from-gray-950 w-full">
      <p className="font-medium">Seja bem-vindo de volta!</p>

      <div className="w-1/6 text-lg  ">
        <Button onClick={openAddMovieModal}>
          <>
            <p className="min-w-fit">Adicionar Filme</p>
            <div className="w-[1.5rem] h-full">
              <AddIcon />
            </div>
          </>
        </Button>
      </div>
    </div>
  );
}

function ListOfMovies({
  deleteMovie,
  editMovie,
  getMovies,
  loading,
  totalPages,
  currentPage,
  movies,
  handleChangeCurrentPage,
}: IListOfMovies) {
  useEffect(() => {
    getMovies();
  }, [currentPage]);

  if (loading) {
    return <>loading</>;
  }

  async function addListMovie(id: string) {
    try {
      await addMovieToList({ id });
      SuccessToast("Adicionado!");
    } catch (error) {
      ErrorToast("Não foi possível adicionar o filme a sua lista.");
    }
  }

  return (
    <div className="bg-gray-950  h-auto flex flex-col min-h-[30vh] pb-10">
      <div className="w-full  flex justify-center  items-start flex-wrap gap-10 px-8 py-10 mx-auto">
        {movies.map((movie) => (
          <MovieCard
            key={movie.id}
            deleteMovie={() => deleteMovie(movie.id)}
            editMovie={() => editMovie(movie)}
            addMovie={() => addListMovie(movie.id)}
            {...movie}
          />
        ))}
      </div>

      <div className="w-full flex justify-center">
        <Pagination
          totalPages={totalPages}
          currentPage={currentPage}
          setCurrentPage={handleChangeCurrentPage}
        />
      </div>
    </div>
  );
}

function MovieForm({ closeModal, openModal, movie, refresh }: IMovieForm) {
  const [banner, setBanner] = useState();

  async function CreateMovie(values: ICreateMovie) {
    return createMovie({ ...values, banner: banner });
  }

  async function EditMovie(values: IEditMovie) {
    return editMovie({ ...values, banner: banner });
  }

  async function handleSubmit(values: ICreateMovie | IEditMovie) {
    try {
      if (movie) {
        await EditMovie({ ...values, id: movie.id });
        SuccessToast("Filme Atualizado!");
        closeModal();
        refresh();
      } else {
        await CreateMovie({ ...values, banner: banner });
        SuccessToast("Filme criado!");
      }
    } catch (error) {
      console.log(error);
      const message = error.response.data;

      if (message == "already_exists") {
        WarmingToast("Já existe um filme com esse nome");
      } else {
        ErrorToast(`Não foi possível ${movie ? "editar" : "criar"} o filme`);
      }
    }
  }

  async function handleFile(_file) {
    const base64 = await getBase64(_file);
    setBanner(base64);
  }

  useEffect(() => {
    if (movie) {
      setBanner(movie.banner);
    }
  }, [movie]);

  return (
    <FormModal
      closeModal={closeModal}
      title={movie ? "Editar Filme" : "Criar Filme"}
      openModal={openModal}
    >
      <Formik
        initialValues={{
          description: movie ? movie.description : "",
          name: movie ? movie.name : "",
          ratings: movie ? movie.ratings : 0,
          releaseDate: movie ? formateDate(movie.releaseDate) : undefined,
        }}
        onSubmit={handleSubmit}
        className="w-full"
      >
        <Form className="flex flex-col gap-4 ">
          <div className="flex flex-col gap-2">
            <label className="font-semibold" htmlFor="name">
              Nome
            </label>
            <Field
              name="name"
              type="text"
              id="name"
              className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-12 px-2 "
            />
          </div>
          <div className="flex flex-col gap-2">
            <label className="font-semibold" htmlFor="description">
              Descrição
            </label>
            <Field
              as="textarea"
              name="description"
              id="description"
              type="textarea"
              className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-12 px-2 "
            />
          </div>

          <div className="grid md:grid-cols-2  gap-8 h-fit ">
            <div className="flex flex-col gap-2">
              <label className="font-semibold" htmlFor="ratings">
                Nota
              </label>

              <Field
                name="ratings"
                id="ratings"
                type="number"
                className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-12 px-2 "
              />
            </div>

            <div className="flex flex-col gap-2">
              <label className="font-semibold" htmlFor="releaseDate">
                Data de Lançamento
              </label>
              <Field
                id="releaseDate"
                name="releaseDate"
                type="date"
                className="w-full  bg-gray-900 border-2 border-slate-700 rounded h-12 px-2 "
              />
            </div>
          </div>

          <div className="h-auto flex flex-col gap-2">
            <label className="font-semibold" htmlFor="banner">
              Capa
            </label>

            <Field
              id="banner"
              name="banner"
              render={({ field }) => (
                <UploadImage
                  handleFile={handleFile}
                  uploadImage={!banner}
                  field={field}
                />
              )}
            />

            <ShowBanner
              banner={banner}
              removeImage={() => setBanner(undefined)}
            />
          </div>

          <button
            type="submit"
            className="bg-primary w-3/6 font-semibold h-12 font-semibold"
          >
            {movie ? "Editar" : "Criar"}
          </button>
        </Form>
      </Formik>
    </FormModal>
  );
}

function RemoveMovieModal({
  confirmeDelete,
  cancelDelete,
  openModal,
}: IRemoveMovieModal) {
  return (
    <Modal openModal={openModal}>
      <div className="w-full flex flex-col items-center gap-8">
        <p className="w-full text-xl text-center">
          Você tem certeza que deseja remover esse filme?
        </p>

        <div className="flex gap-4 w-3/6">
          <Button onClick={confirmeDelete} isToRemove>
            Confirmar
          </Button>
          <Button onClick={cancelDelete}>Cancelar</Button>
        </div>
      </div>
    </Modal>
  );
}

function FormModal({ openModal, children, title, closeModal }: IFormModal) {
  if (!openModal) {
    return <></>;
  }

  return (
    <Modal openModal={openModal}>
      <>
        {title && (
          <div className="w-full flex justify-between">
            <p className="text-xl my-2  font-semibold">{title}</p>
            <div className="cursor-pointer" onClick={closeModal}>
              <XIcon />
            </div>
          </div>
        )}

        {children}
      </>
    </Modal>
  );
}

function ShowBanner({ banner, removeImage }: IShowBanner) {
  if (!banner) {
    return <></>;
  }
  return (
    <div className="w-full h-auto flex justify-between">
      <img src={banner} className="w-20 h-auto" />
      <div className="w-2/6">
        <Button onClick={() => removeImage(undefined)} isToRemove>
          Remover Imagem
        </Button>
      </div>
    </div>
  );
}

function UploadImage({ uploadImage, handleFile, field }: IUploadImage) {
  if (!uploadImage) {
    return <></>;
  }

  return (
    <input
      {...field}
      id="banner"
      name="banner"
      type="file"
      onChange={(e) => {
        handleFile(e.target.files[0]);
      }}
      className="w-full h-14 bg-gray-900 py-3 border-2 border-slate-700 rounded h-12 px-2"
    />
  );
}

function Modal({ children, openModal }: IModal) {
  if (!openModal) {
    return <></>;
  }

  return (
    <div className="fixed top-0 z-10 w-full h-screen flex justify-center items-center bg-[#00000095]">
      <div className="px-8 py-8 w-4/6 h-auto flex flex-col bg-gray-900 shadow-lg rounded">
        {children}
      </div>
    </div>
  );
}

// ======================= types ============================
interface IListOfMovies {
  deleteMovie: (_id: string) => unknown;
  editMovie: (_movie: object) => unknown;
  getMovies: () => unknown;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  movies: IMovieCard[];
  handleChangeCurrentPage: (page: number) => unknown;
}
interface IMovieForm {
  closeModal: () => unknown;
  openModal: boolean;
  movie?: IMovieCard;
  refresh: () => unknown;
}
interface IRemoveMovieModal {
  openModal: boolean;
  confirmeDelete: () => unknown;
  cancelDelete: () => unknown;
}
interface IFormModal {
  title?: string;
  openModal: boolean;
  closeModal: () => unknown;
  children: JSX.Element | JSX.Element[];
}
interface IShowBanner {
  banner: string | undefined;
  removeImage: (_: undefined) => unknown;
}
interface IUploadImage {
  uploadImage: boolean;
  handleFile: (file: File) => unknown;
  field: object;
}
interface IModal {
  children: JSX.Element | JSX.Element[];
  openModal: boolean;
}
// ======================= types ============================
