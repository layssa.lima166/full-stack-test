import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { addMovieToList, getMovieById } from "../../../server";
import { Button } from "../../../components/button";
import { MovieAverage } from "../../../components/movieAverage";
import { ErrorToast, SuccessToast } from "../../../components/toasts";

interface IMovieData {
  id: string;
  banner: string;
  name: string;
  description: string;
  ratings: number;
  releaseDate: string;
}

export default function MovieDetails() {
  const [movieData, setMovieData] = useState<IMovieData | undefined>();
  const { id } = useParams();

  async function getUserData() {
    try {
      const userData = await getMovieById({ id });

      setMovieData(userData.data);
    } catch (error) {
      console.log(error);
    }
  }

  async function addListMovie(id: string) {
    try {
      await addMovieToList({ id });
      SuccessToast("Adicionado!");
    } catch (error) {
      ErrorToast("Não foi possível adicionar o filme a sua lista.");
    }
  }

  useEffect(() => {
    void getUserData();
  }, []);

  if (!movieData) {
    return (
      <div className="w-full flex justify-center px-10 py-10">
        <div className="px-8 py-8 w-full h-auto flex flex-col gap-4 bg-gray-800 shadow-lg rounded">
          Carregando
        </div>
      </div>
    );
  }

  const year = new Date(movieData?.releaseDate).getFullYear();

  return (
    <div className="relative w-full ">
      <div className=" flex justify-center items-start  gap-8 inset-y-0 gap-8  w-full h-full md:h-[85vh]  overflow-hidden bg-gradient-to-t from-gray-950">
        <div className="h-auto w-10/12 flex justify-center items-start gap-8 py-20">
          <img src={movieData?.banner} />

          <div className=" w-8/12 flex flex-col items-between gap-10">
            <div>
              <p className="text-2xl font-semibold">{movieData?.name}</p>
              <div>
                <span className="left">{year}</span>

                <MovieAverage rating={movieData?.ratings} />
              </div>
            </div>

            <p className="w-5/6 text-sm">{movieData?.description}</p>

            <div className="w-2/6">
              <Button onClick={() => addListMovie(id)}>
                Adicionar a sua lista
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
