import { useContext, useEffect, useState } from "react";
import profileImage from "../../../assets/profile.webp";
import { getUserById } from "../../../server";
import { AuthContext } from "../../../context/auth";

interface IUserData {
  email: string;
  id: string;
  type: "ADMIN" | "User";
  name: string;
  movies: Array<object>;
}

export default function MyProfile() {
  const [userData, setUserData] = useState<IUserData | undefined>();
  const { user } = useContext(AuthContext);

  async function getUserData() {
    try {
      const userData = await getUserById({ id: user.id });

      setUserData(userData.data);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    void getUserData();
  }, []);

  if (!userData) {
    <div className="w-full flex justify-center px-10 py-10">
      <div className="px-8 py-8 w-full h-auto flex flex-col gap-4 bg-gray-800 shadow-lg rounded">
        Carregando
      </div>
    </div>;
  }

  return (
    <div className="w-full flex justify-center px-10 py-10">
      <div className="px-8 py-8 w-full h-auto flex flex-col gap-4 bg-gray-800 shadow-lg rounded">
        <p className="text-lg font-semibold my-4">Meu perfil</p>

        <div className="w-20 h-20">
          <img src={profileImage} className="w-full h-full rounded" />
        </div>

        <div className="w-full  flex flex-col gap-2">
          <p className=""> Email: {userData?.email} </p>
          <p className="">
            {" "}
            Tipo de usário: {UserTypeTranslater[userData?.type]}{" "}
          </p>
          <p className=""> Filmes: {userData?.movies.length} </p>
        </div>
      </div>
    </div>
  );
}

const UserTypeTranslater = {
  ADMIN: "Administrador",
  USER: "Cliente",
};
