import { useEffect, useState } from "react";

import { ErrorToast } from "../../../components/toasts";

import { getAllUsers } from "../../../server";
import { Pagination } from "../../../components/Pagination";

export default function Users() {
  return (
    <div className="w-full h-auto flex flex-col items-start">
      <ListOfUsers />
    </div>
  );
}

function ListOfUsers() {
  const [loading, setLoading] = useState(true);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);
  const [users, setUsers] = useState([]);

  async function getMyListMovies() {
    try {
      const response = await getAllUsers({
        pageSize: 12,
        pageOffset: currentPage,
      });

      if (response.data) {
        setUsers(response.data.data);

        const totalPages_ = Math.ceil(response.data.totalItems / 12);

        setTotalPages(totalPages_);
        setLoading(false);
      }
    } catch (error) {
      console.log(error);
      ErrorToast("Ocorreu um erro ao tentar carregar os filmes.");
      setLoading(false);
    }
  }

  useEffect(() => {
    getMyListMovies();
  }, [currentPage]);

  if (loading) {
    return <>loading</>;
  }

  if (users.length == 0) {
    return <EmptyList />;
  }

  return (
    <div className="bg-gradient-to-t from-gray-950 w-full min-h-[85vh] h-auto flex flex-col py-10 ">
      <div className="w-full h-14 px-4 py-2 grid grid-cols-3 justify-items-center">
        <p className="text-lg">Nome do Usuário</p>

        <p className="text-lg">Email</p>

        <p className="text-lg">Filmes Salvos</p>
      </div>

      <div className="w-full  h-auto flex  justify-center  items-center flex-col gap-10 px-8 py-10 mx-auto">
        {users.map((user) => (
          <div className="w-full h-14 px-4 py-2 grid grid-cols-3 justify-items-center content-center rounded bg-transparent border-[1px] border-gray-200">
            <p className="h-fit">{user.name}</p>

            <p className="h-fit">{user.email}</p>

            <p className="h-fit">{user.movies.length}</p>
          </div>
        ))}

          <Pagination
            totalPages={totalPages}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
      </div>
    </div>
  );
}

function EmptyList() {
  return (
    <div className="w-full py-8 flex justify-center items-center  gap-8 inset-y-0 gap-8  w-full h-full md:h-[85vh]  overflow-hidden bg-gradient-to-t from-gray-950">
      Lista vazia
    </div>
  );
}
