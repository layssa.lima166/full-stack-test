import { useEffect, useState } from "react";

import { ErrorToast, SuccessToast } from "../../../components/toasts";

import { MovieCard } from "../../../components/movieCard";
import { addMovieToList, getMyList } from "../../../server";
import { Pagination } from "../../../components/Pagination";

export default function MyList() {
  return (
    <div className="w-full h-auto flex flex-col items-start">
      <ListOfMovies />
    </div>
  );
}

function ListOfMovies() {
  const [loading, setLoading] = useState(true);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);
  const [movies, setMovies] = useState([]);

  async function getMyListMovies() {
    try {
      const response = await getMyList({
        pageSize: 12,
        pageOffset: currentPage,
      });

      if (response.data) {
        setMovies(response.data.data);

        const totalPages_ = Math.ceil(response.data.totalItems / 12);

        setTotalPages(totalPages_);
        setLoading(false);
      }
    } catch (error) {
      console.log(error);
      ErrorToast("Ocorreu um erro ao tentar carregar os filmes.");
      setLoading(false);
    }
  }

  async function addListMovie(id: string) {
    try {
      await addMovieToList({ id });
      SuccessToast("Adicionado!");
    } catch (error) {
      ErrorToast("Não foi possível adicionar o filme a sua lista.");
    }
  }

  useEffect(() => {
    getMyListMovies();
  }, [currentPage]);

  if (loading) {
    return <>loading</>;
  }

  if (movies.length == 0) {
    return <EmptyList />;
  }

  return (
    <div className="bg-gradient-to-t from-gray-950 w-full min-h-[85vh] h-auto flex pb-10 ">
      <div className="w-full  h-auto flex  justify-center  items-start flex-wrap gap-10 px-8 py-10 mx-auto">
        {movies.map((movie) => (
          <MovieCard
            key={movie.id}
            addMovie={() => addListMovie(movie.id)}
            isToRemove
            {...movie}
          />
        ))}

        <div className="w-full flex justify-center">
          <Pagination
            totalPages={totalPages}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        </div>
      </div>
    </div>
  );
}

function EmptyList() {
  return (
    <div className="w-full py-8 flex justify-center items-center  gap-8 inset-y-0 gap-8  w-full h-full md:h-[85vh]  overflow-hidden bg-gradient-to-t from-gray-950">
      Lista vazia
    </div>
  );
}
