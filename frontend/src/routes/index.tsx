import { Navigate, createBrowserRouter } from "react-router-dom";
import Login from "../pages/auth/login";
import { AuthLayout } from "../components/layouts/auth";
import { PlatformLayout } from "../components/layouts/platform";
import Home from "../pages/platform/home";
import MyProfile from "../pages/platform/profile";
import MovieDetails from "../pages/platform/details";
import Register from "../pages/auth/register";
import MyList from "../pages/platform/personalList";
import Users from "../pages/platform/users";
import ProtectedRoute from "./protectedRouter";
import { RootRedirect } from "./rootRedirect";

export const routers = createBrowserRouter([
  {
    path: "/",
    element: <RootRedirect />,
  },
  {
    path: "/",
    element: <AuthLayout />,
    children: [
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/register",
        element: <Register />,
      },
    ],
  },
  {
    path: "/",
    element: (
      <ProtectedRoute>
        <PlatformLayout />
      </ProtectedRoute>
    ),
    children: [
      {
        path: "/platform",
        element: <Home />,
      },
      {
        path: "/platform/profile",
        element: <MyProfile />,
      },
      {
        path: "/platform/:id",
        element: <MovieDetails />,
      },
      {
        path: "/platform/list",
        element: <MyList />,
      },
      {
        path: "/platform/users",
        element: <Users />,
      },
    ],
  },
]);
