import { useContext } from "react";
import { AuthContext } from "../context/auth";
import { Navigate } from "react-router-dom";

export function RootRedirect() {
  const { authenticated } = useContext(AuthContext);
  return authenticated ? <Navigate to="/platform" /> : <Navigate to="/login" />;
}
