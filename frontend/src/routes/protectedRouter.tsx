import { Navigate } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../context/auth";

const ProtectedRoute = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => {
  const { authenticated } = useContext(AuthContext);

  if (!authenticated) {
    return <Navigate to="/login" />;
  }

  return children;
};

export default ProtectedRoute;
