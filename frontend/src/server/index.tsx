import axios from "axios";

const api = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL
});

api.interceptors.request.use(
   function (config) {
    const auth = localStorage.getItem("auth");

    config.headers.authorization = auth;

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export interface IAuthLogin {
  email: string;
  password: string;
}

export interface IAuthRegister {
  email: string;
  name: string;
  password: string;
}

export interface IAuthRegister {
  name: string;
  email: string;
  password: string;
  repeatPassword: string;
}

async function loginRequest(loginData: IAuthLogin) {
  return api.post("/login", { ...loginData });
}

async function registerRequest(loginData: IAuthRegister) {
  return api.post("/register", { ...loginData });
}

async function getAllMovies({
  pageSize,
  pageOffset,
}: {
  pageSize: number;
  pageOffset: number;
}) {
  return api.get(`/movies?pageSize=${pageSize}&pageOffset=${pageOffset}`);
}

async function getMovieById({ id }: { id: string }) {
  return api.get(`/movies/${id}`);
}

async function createMovie({ ...movie }: ICreateMovie) {
  return api.post(`/movies/create`, movie);
}

async function editMovie({ ...movieEdit }: IEditMovie) {
  return api.put(`/movies/update`, movieEdit);
}

async function deleteMovie({ id }: { id: string }) {
  return api.delete(`/movies/delete/${id}`);
}

async function getUserById({ id }: { id: string }) {
  return api.get(`/user/${id}`);
}

async function addMovieToList({ id }: { id: string }) {
  return api.put(`/list/movie/${id}`);
}

async function getMyList({
  pageSize,
  pageOffset,
}: {
  pageSize: number;
  pageOffset: number;
}) {
  return api.get(`/list?pageSize=${pageSize}&pageOffset=${pageOffset}`);
}

async function getAllUsers({
  pageSize,
  pageOffset,
}: {
  pageSize: number;
  pageOffset: number;
}) {
  return api.get(`/user?pageSize=${pageSize}&pageOffset=${pageOffset}`);
}

export {
  loginRequest,
  registerRequest,
  getMovieById,
  getAllMovies,
  createMovie,
  editMovie,
  deleteMovie,
  getUserById,
  addMovieToList,
  getMyList,
  getAllUsers,
};

export interface ICreateMovie {
  name: string;
  description: string;
  ratings: string;
  releaseDate: string;
  banner?: string;
}

export interface IEditMovie {
  id: string;
  name?: string;
  description?: string;
  ratings?: string;
  releaseDate?: string;
  banner?: string;
}
