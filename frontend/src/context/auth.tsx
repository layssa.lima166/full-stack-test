import React, { useEffect, useState } from "react";
import {
  IAuthLogin,
  IAuthRegister,
  loginRequest,
  registerRequest,
} from "../server";

export const AuthContext = React.createContext({
  id: null,
  user: null,
  authenticated: false,
  loading: false,
});

export function AuthProvider({ children }: { children: JSX.Element }) {
  const [authenticated, setAuthenticated] = useState<boolean>(
    Boolean(localStorage.getItem("auth")) || false
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [user, setUser] = useState<object | null>({});

  useEffect(() => {
    setLoading(false);
    const userStoraged = JSON.parse(localStorage.getItem("user"));
    
    if (userStoraged) {
      setUser(userStoraged);
      setAuthenticated(true);
    }
  }, []);

  useEffect(() => {
    if (user?.token) {
      localStorage.setItem("user", JSON.stringify(user));
    }
  }, [user]);

  // ------------------------- Login -------------------------
  const handleLogin = async (values: IAuthLogin) => {
    const response = await loginRequest(values);

    localStorage.setItem("auth", "true");

    setAuthenticated(true);

    setUser(response.data);

    return response;
  };

  // ------------------------- Logout -------------------------
  const handleLogout = async () => {
    setAuthenticated(false);
    setUser(null);
    setLoading(false);

    localStorage.clear();
  };

  // ------------------------- Sign Up -------------------------
  const handleSignUp = async (values: IAuthRegister) => {
    const response = await registerRequest(values);

    if (!response.data) {
      setAuthenticated(false);
      throw "Erro";
    }

    setUser(response.data);
    localStorage.setItem("token", response.data.token);
    return response;
  };

  return (
    <AuthContext.Provider
      value={{
        user: user,
        authenticated,
        loading,
        setUser,
        setAuthenticated,
        handleLogin,
        handleLogout,
        handleSignUp,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}
