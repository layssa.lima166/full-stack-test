import { useEffect, useState } from "react";
import { Button } from "./button";
import { ChevronLeftIcon, ChevronRightIcon } from "./icons/chevron";

interface IPagination {
  totalPages: number;
  currentPage: number;
  setCurrentPage: (_next: number) => void;
}

export function Pagination({
  currentPage,
  setCurrentPage,
  totalPages,
}: IPagination) {
  const [pagesSlices, setPagesSlices] = useState([]);
  const PagesBtn = Array.from({ length: totalPages }, (_, index) => index + 1);

  function cutArray() {
    const maxOfPages = totalPages;
    const endofArray = currentPage + 8;

    if (endofArray >= maxOfPages && currentPage == totalPages) {
      const pagesSlice_ = PagesBtn.slice(currentPage - 8, currentPage);
      setPagesSlices(pagesSlice_);
    } else {
      const pagesSlice_ = PagesBtn.slice(currentPage, currentPage + 8);

      setPagesSlices(pagesSlice_);
    }
  }

  useEffect(() => {
    cutArray();
  }, [currentPage, totalPages]);

  useEffect(() => {
    cutArray();
  }, []);

  function handlePreviousPage() {
    setCurrentPage((prev) => {
      if (prev > 0) {
        const previousNumber = prev - 1;
        return previousNumber;
      } else return prev;
    });
  }

  function handleNextPage() {
    setCurrentPage((prev) => {
      if (prev != totalPages) {
        const nextNumber = prev + 1;
        return nextNumber;
      }
      return prev;
    });
  }

  function handleClickAtPageNumber(_page: number) {
    setCurrentPage(_page);
  }

  if (pagesSlices.length == 0) {
    return <></>;
  }

  return (
    <div className="flex justify-center my-4">
      <div className="inline-flex space-x-2">
        <Button onClick={handlePreviousPage}>
          <ChevronLeftIcon />
        </Button>

        {pagesSlices.map((page: number) => (
          <div
            key={page}
            className={`${page == currentPage + 1 ? "bg-primary" : ""}`}
          >
            <Button
              onClick={() => {
                handleClickAtPageNumber(page - 1);
              }}
            >
              {page}
            </Button>
          </div>
        ))}

        <Button onClick={handleNextPage}>
          <ChevronRightIcon />
        </Button>
      </div>
    </div>
  );
}
