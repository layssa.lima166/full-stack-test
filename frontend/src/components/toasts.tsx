import { toast } from "react-toastify";

const toastStyled = {
  position: "top-right",
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
  theme: "dark",
  
} as typeof toast.arguments;

function SuccessToast(message: string) {
  return toast.success(message, toastStyled);
}

function ErrorToast(message: string) {
  return toast.error(message, toastStyled);
}

function WarmingToast(message: string) {
  return toast.warn(message, toastStyled);
}

export { SuccessToast, ErrorToast, WarmingToast };
