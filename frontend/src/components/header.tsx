import { useNavigate } from "react-router-dom";
import logo from "../assets/logo.png";
import profileImage from "../assets/profile.webp";
import { useContext } from "react";
import { AuthContext } from "../context/auth";

export function Header() {
  const { handleLogout, user } = useContext(AuthContext);
  const navigate = useNavigate();

  function goToMyProfile() {
    navigate("/platform/profile");
  }

  function goToHome() {
    navigate("/platform");
  }

  function goToMyList() {
    navigate("/platform/list");
  }

  function goToUsersList() {
    navigate("/platform/users");
  }

  function logout() {
    navigate("/login");
    handleLogout();
  }

  return (
    <head className="w-full flex justify-between gap-10 px-4 py-4">
      <img src={logo} />

      <nav className="w- flex justify-start self-center items-center gap-10 ">
        <a className="text-lg text-white cursor-pointer" onClick={goToHome}>
          Home
        </a>
        <a className="text-lg text-white cursor-pointer" onClick={goToMyList}>
          Minha Lista
        </a>
        {user.type == "ADMIN" && (
          <a
            className="text-lg text-white cursor-pointer"
            onClick={goToUsersList}
          >
            Usuários
          </a>
        )}
      </nav>

      <div className="flex gap-4 items-center w-1/12">
        <p className="text-gray-300 cursor-pointer" onClick={logout}>
          Sair
        </p>

        <div
          onClick={goToMyProfile}
          className="bg-red-800 w-14 h-14 rounded-full cursor-pointer"
        >
          <img src={profileImage} className="min-w-14  w-full h-full rounded-full" />
        </div>
      </div>
    </head>
  );
}
