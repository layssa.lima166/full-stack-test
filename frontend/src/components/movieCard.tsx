import { useNavigate } from "react-router-dom";
import { Button } from "./button";
import { EditIcon } from "./icons/edit";
import { TrashIcon } from "./icons/trash";
import { useContext, useState } from "react";
import { MovieAverage } from "./movieAverage";
import { AuthContext } from "../context/auth";
import { AddIcon } from "./icons/add";
import { XIcon } from "./icons/x";

interface IMovieCardFunctions extends IMovieCard {
  deleteMovie: () => unknown;
  editMovie: () => unknown;
  addMovie: () => unknown;
  isToRemove?: boolean;
}
export interface IMovieCard {
  id: string;
  banner: string;
  name: string;
  description: string;
  ratings: number;
  releaseDate: Date;
}

export function MovieCard({ ...movie }: IMovieCardFunctions) {
  const {
    id,
    banner,
    name,
    ratings,
    releaseDate,
    deleteMovie,
    editMovie,
    isToRemove,
    addMovie,
  } = movie;

  const movieYearLaunched = new Date(releaseDate).getFullYear();
  const [seeDetails, setSeeDetails] = useState(false);

  const { user } = useContext(AuthContext);

  const navigate = useNavigate();

  function seeMovieDetails() {
    navigate(`/platform/${id}`);
  }

  return (
    <div
      onMouseEnter={() => {
        setSeeDetails(true);
      }}
      onMouseLeave={() => {
        setSeeDetails(false);
      }}
      className={`relative w-full max-w-[18rem] md:w-[18rem]  min-h-[24rem]  rounded flex flex-col gap-4 shadow-2xl `}
    >
      <img
        src={banner}
        className="rounded max-h-[24rem] min-h-full min-w-full"
      />
      <div className="absolute z-0 h-full w-full flex flex-col justify-between py-4 gap-2 rounded bg-gradient-to-t from-[#000000]  to-[#00000030]">
        <HeaderBtns
          isToRemove={isToRemove}
          addListMovie={addMovie}
          deleteMovie={deleteMovie}
          editMovie={editMovie}
          userType={user.type}
        />

        {seeDetails && (
          <div className=" cursor-pointer flex justify-self-end justify-center items-center  w-full  rounded z-10 0] px-10">
            <Button onClick={seeMovieDetails}>Ver Detalhes</Button>
          </div>
        )}

        <div className="px-4 ">
          <MovieAverage rating={ratings} />
          <p className="text-text-gray font-semibold text-xl">{name}</p>
          <p className="text-text-gray font-semibold text-">
            {movieYearLaunched}
          </p>
        </div>
      </div>
    </div>
  );
}

function HeaderBtns({
  userType,
  deleteMovie,
  editMovie,
  addListMovie,
  isToRemove,
}: {
  addListMovie: () => void;
  editMovie: () => void;
  deleteMovie: () => void;
  userType: "ADMIN" | "USER";
  isToRemove?: boolean;
}) {
  if (isToRemove) {
    return (
      <div className=" flex gap-4 w-full md:w-2/6 px-4">
        <Button isToRemove onClick={addListMovie}>
          <XIcon />
        </Button>
      </div>
    );
  }

  if (userType == "USER") {
    return (
      <div className=" flex gap-4 w-full md:w-2/6 px-4">
        <Button onClick={addListMovie}>
          <AddIcon />
        </Button>
      </div>
    );
  }

  return (
    <div className=" flex gap-4 w-full md:w-4/6 px-4">
      <Button onClick={editMovie}>
        <EditIcon />
      </Button>

      <Button onClick={deleteMovie} isToRemove>
        <TrashIcon />
      </Button>
    </div>
  );
}
