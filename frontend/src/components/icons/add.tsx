export function AddIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      stroke="currentColor"
      stroke-width="2"
      stroke-linecap="round"
      stroke-linejoin="round"
      className="lucide lucide-plus w-full h-full"
    >
      <path d="M5 12h14" />
      <path d="M12 5v14" />
    </svg>
  );
}
