export function Button({
    children,
    isToRemove,
    onClick,
  }: {
    children: JSX.Element | string;
    isToRemove?: boolean;
    onClick: () => unknown;
  }) {
    return (
      <button
        onClick={onClick}
        className={`w-full  h-12 min-w-20 font-semibold flex justify-center gap-4 items-center font-medium rounded hover:bg-primary hover:border-primary bg-transparent border-[1px] border-white ${
          isToRemove ? "hover:bg-red-600" : "hover:bg-primary"
        }`}
      >
        {children}
      </button>
    );
  }
  