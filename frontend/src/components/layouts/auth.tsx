import { Outlet } from "react-router-dom";

export function AuthLayout() {
  return (
    <div className="w-screen h-auto min-h-screen bg-gray-950 flex justify-center items-center">
      <div className="h-5/6 w-5/6 md:w-4/6 flex flex-col justify-center items-center shadow-lg  py-10">
        <Outlet />
      </div>
    </div>
  );
}
