import { Outlet } from "react-router-dom";
import { Header } from "../header";

export function PlatformLayout() {
  return (
    <div className="w-full bg-gray-900 h-auto min-h-screen">
      <Header />

      <Outlet />
    </div>
  );
}
