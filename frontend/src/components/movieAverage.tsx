export function MovieAverage({ rating }: { rating: number }) {

  const ratingFormated = rating.toFixed(2);

  return (
    <p className="my-2 text-gray-200 text-sm font-semibold w-[3rem] h-fit py-[1px] flex justify-center items-center rounded border-[1px] border-gray-200">
      {ratingFormated}
    </p>
  );
}
