export function formateDate(_date: string) {
  const data = new Date(_date);
  const year: number = data.getFullYear();
  const month: number = data.getMonth();
  const day: number = data.getDay();

  const year_ = (year > 10 ? year : `0${year}`) as string;
  const month_ = (month > 10 ? month : `0${month}`) as string;
  const day_ = (day > 10 ? day : `0${day}`) as string;

  return `${year_}-${month_}-${day_}`;
}
