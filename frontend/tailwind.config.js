/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: "#0540F2",
        "primary10": "#0540F210",
        "primary40": "#0540F240",
        "primary-hover": "#0034D2",
        "bg-light": "#f9f9f9",
        "text-black": "#1D1D1D",
        "text-secondary": "#737373",
        black: "#171717",
        icons: "#3C3C3C",
        subtopics: "#A5A5A5",
        error: '#E71818',
        warming: '#D97904',
        warming2: '#F2CB07',
        white2: '#F9F9F9'
      },
      fontFamily: {
        sans: ["var(--font-dm-sans)"],
      },
    },
  },
  plugins: [],
}

