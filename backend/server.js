//----------- DEPENDÊNCIAS ------------
const PORT = 8000;

import cors from "cors";
import express from "express";
import { routers } from "./src/routes/routes.js";


const app = express();

// -----------------------------------------------------------------------------------------------------//
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());

app.use(routers);

// -----------------------------------------------------------------------------------------------------//
app.listen(process.env.PORT || PORT, function (err) {
  if (err) console.log(err);
  console.log("Server listening on PORT", PORT);
});