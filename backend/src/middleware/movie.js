import { createMovieValidation, deleteMovieValidation, updateMovieValidation } from "../validators/movie.js";

export async function createMovieMiddleware(req, res, next) {
  try {
    await createMovieValidation.validate({
      body: req.body,
    });
    return next();
  } catch (err) {

    return res.status(400).send({
      message: "Invalid Data",
      code: 500,
    });
  }
}

export async function updateMovieMiddleware(req, res, next) {
  try {
    await updateMovieValidation.validate({
      body: req.body,
    });
    return next();
  } catch (err) {

    return res.status(400).send({
      message: "Invalid Data",
      code: 500,
    });
  }
}

export async function deleteMovieMiddleware(req, res, next) {
  try {
    await deleteMovieValidation.validate({
      params: req.params,
    });
    return next();
  } catch (err) {

    return res.status(400).send({
      message: "Invalid Data",
      code: 500,
    });
  }
}
