import { authenticJWT } from "../utils/auth-jwt.js";

export async function tokenValidation(req, res, next) {
  const authorization = req.header('authorization');
  
  const authResponse = await authenticJWT(authorization);

  if (authResponse.auth) {
    req.id = authResponse._token.id;
    next();
  } else {
    return res.send(authResponse);
  }
}
