import { createUserValidation } from "../validators/user.js";

export async function createUserMiddleware(req, res, next) {
  try {
    await createUserValidation.validate({
      body: req.body,
    });

    return next();
  } catch (err) {
    return res.status(400).send({
      message: "Invalid Data",
      code: 500,
    });
  }
}
