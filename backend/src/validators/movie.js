import yup from "yup";

export const createMovieValidation = yup.object({
  body: yup.object({
    name: yup.string().required(),
    description: yup.string().required(),
    ratings: yup.number().required(),
    releaseDate: yup.date().required(),
    banner: yup.string().optional(),
  }),
});

export const updateMovieValidation = yup.object({
  body: yup.object({
    id: yup.string().required(),
    description: yup.string().optional(),
    ratings: yup.number().optional(),
    releaseDate: yup.date().optional(),
    banner: yup.string().optional(),
  }),
});


export const deleteMovieValidation = yup.object({
  params: yup.object({
    id: yup.string().required(),
  }),
});
