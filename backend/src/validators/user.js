import yup from "yup";

export const createUserValidation = yup.object({
  body: yup.object({
    name: yup.string().required(),
    email: yup.string().required(),
    password: yup.string().required(),
    repeatPassword: yup.string().required(),
  }),
});
