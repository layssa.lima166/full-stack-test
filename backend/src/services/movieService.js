import {
  createMovie,
  deleteMovie,
  getMovieByString,
  updateMovie,
  countTotalMovies,
  getAllMovies,
  getMovieById,
} from "../repositories/movies.js";

async function createMovieService(movieData) {
  const alreadyExists = await getMovieByString(movieData.name);

  if (alreadyExists) {
    throw "already_exists";
  }

  return createMovie(movieData);
}

async function deleteMovieService(movieId) {
  return deleteMovie(movieId);
}

async function updateMovieService(movieData) {
  return updateMovie(movieData);
}

async function getAllMoviesService({ pageSize, pageOffset }) {
  const movies = await getAllMovies({ pageSize, pageOffset });
  const totalItems = await countTotalMovies();
  return { data: movies, totalItems };
}
async function getMovieByIdService({ id }) {
  return await getMovieById({ id });
}


export {
  createMovieService,
  deleteMovieService,
  getAllMoviesService,
  updateMovieService,
  getMovieByIdService,
};
