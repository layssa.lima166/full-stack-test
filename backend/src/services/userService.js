import {
  countTotalMoviesListOwner,
  getListMoviesOwner,
} from "../repositories/movies.js";
import {
  addMovieToUserList,
  countUsers,
  getAllUsers,
  getUserById,
} from "../repositories/user.js";

async function getUserByIdService({ id }) {
  const user = await getUserById(id);

  if (!user) {
    throw "user_not_found";
  }

  delete user.password;
  delete user.salt;

  return user;
}

async function getUserMoviesListService({ id, pageSize, pageOffset }) {
  const movies = await getListMoviesOwner({ pageSize, pageOffset, id });
  const totalItems = await countTotalMoviesListOwner(id);

  if (!movies) {
    throw "user_not_found";
  }

  return { data: movies, totalItems: totalItems };
}

async function addMovieToUserListService(userId, movieId) {
  const userUpdated = await addMovieToUserList(userId, movieId);

  if (!userUpdated) {
    throw "user_not_found";
  }

  delete userUpdated.password;
  delete userUpdated.salt;

  return userUpdated;
}

async function getAllUsersServices({ pageSize, pageOffset }) {
  const users = await getAllUsers({ pageSize, pageOffset });
  const totalItems = await countUsers();

  return { data: users, totalItems: totalItems };
}

export {
  getUserByIdService,
  addMovieToUserListService,
  getUserMoviesListService,
  getAllUsersServices,
};
