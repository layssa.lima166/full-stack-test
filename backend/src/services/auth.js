import { authErrors, responseMessages } from "../constants/index.js";
import { createUser, getUserByEmail } from "../repositories/auth.js";
import { createToken } from "../utils/auth-jwt.js";
import { authenticateUser, encryptData } from "../utils/hash.js";

async function loginService(userData) {
  const userfounded = await getUserByEmail(userData.email);

  if (userfounded) {
    const authUser = await authenticateUser(
      userData.password,
      userData.email,
      userfounded.password
    );

    if (authUser) {
      const { token } = createToken({
        id: userfounded.id,
        data: new Date(),
      });

      return {
        token: token,
        id: userfounded.id,
        type: userfounded.type,
      };
    } else {
      throw authErrors.incorrect_data;
    }
  } else {
    throw authErrors.user_not_found;
  }
}

async function createUserService(userData) {
  const { hash, salt } = await encryptData(userData.password, userData.email);

  delete userData.repeatPassword;

  const alreadyExist = await getUserByEmail(userData.email);

  if (alreadyExist) {
    throw authErrors.user_already_exits;
  }

  const userCreated = await createUser({
    ...userData,
    password: hash,
    salt: salt,
    type: "USER",
  });

  if (!userCreated) {
    throw responseMessages.internal_server_error;
  }

  const { token } = createToken({
    id: userCreated.id,
    data: new Date(),
  });

  delete userCreated.password;
  delete userCreated.salt;

  return {
    token: token,
    id: userCreated.id,
    type: userCreated.type,
  };
}

export { loginService, createUserService };
