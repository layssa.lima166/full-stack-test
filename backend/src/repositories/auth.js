import { db } from "../db/index.js";

async function getUserByEmail(email){
    return db.user.findUnique({
        where: {
            email: email,
        }
    });
}

async function createUser(userData){
    return db.user.create({
        data: {
            ...userData
        }
    });
}

export { getUserByEmail, createUser }