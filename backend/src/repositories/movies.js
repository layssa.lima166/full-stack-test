import { db } from "../db/index.js";

function getAllMovies({ pageSize, pageOffset }) {
  const pageOffset_ = Number(pageOffset);
  const pageSize_ = Number(pageSize);

  return db.movies.findMany({
    where: {
      deletedAt: null,
    },
    take: Number(pageSize_),
    skip: pageSize_ * pageOffset_,
    orderBy: {
      ratings: "desc",
    },
  });
}

function getMovieById({ id }) {
  return db.movies.findUnique({
    where: {
      deletedAt: null,
      id: id,
    },
  });
}

async function countTotalMovies() {
  return db.movies.count({
    where: {
      deletedAt: null,
    },
  });
}

async function getListMoviesOwner({ pageSize, pageOffset, id }) {
  const pageOffset_ = Number(pageOffset);
  const pageSize_ = Number(pageSize);

  return db.movies.findMany({
    where: {
      users: {
        some: {
          id: id
        },
      },
    },
    take: Number(pageSize_),
    skip: pageSize_ * pageOffset_,
    orderBy: {
      ratings: "desc",
    },
  });
}

async function countTotalMoviesListOwner(id) {
  return db.movies.count({
    where: {
      id: id,
      users: {
        some: {
          id: id
        },
      },
    },
  });
}

async function createMovie(movieData) {
  const releaseDate = new Date(movieData.releaseDate);

  delete movieData.releaseDate;

  return db.movies.create({
    data: {
      ...movieData,
      releaseDate: releaseDate,
    },
  });
}

async function getMovieByString(name) {
  return db.movies.findFirst({
    where: {
      deleteAt: null,
      name: {
        contains: name,
      },
    },
  });
}

async function updateMovie(movieData) {
  const releaseDate = movieData.releaseDate
    ? new Date(movieData.releaseDate)
    : undefined;

  delete movieData.releaseDate;

  return db.movies.update({
    where: {
      id: movieData.id,
    },
    data: {
      ...movieData,
      releaseDate: releaseDate,
    },
  });
}

async function deleteMovie(movieData) {
  return db.movies.update({
    where: {
      id: movieData,
    },
    data: {
      deletedAt: new Date(),
    },
  });
}

export {
  getAllMovies,
  getMovieById,
  countTotalMovies,
  createMovie,
  getMovieByString,
  updateMovie,
  deleteMovie,
  getListMoviesOwner,
  countTotalMoviesListOwner,
};
