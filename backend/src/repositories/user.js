import { db } from "../db/index.js";

async function getUserById(id) {
  return db.user.findUnique({
    where: {
      id: id,
    },
    include: {
      movies: true,
    },
  });
}

async function addMovieToUserList({ userId, movieId }) {
  return db.user.update({
    where: {
      id: userId,
    },
    data: {
      movies: {
        connect: {
          id: movieId,
        },
      },
    },
  });
}

async function getAllUsers({ pageOffset, pageSize }) {
  const pageOffset_ = Number(pageOffset);
  const pageSize_ = Number(pageSize);

  return db.user.findMany({
    include: {
      movies: true,
    },
    take: Number(pageSize_),
    skip: pageSize_ * pageOffset_,
  });
}

async function countUsers() {
  return db.user.count();
}

export { getUserById, addMovieToUserList, getAllUsers, countUsers };
