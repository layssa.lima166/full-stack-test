import { Router } from "express";

import {
  createMovieMiddleware,
  deleteMovieMiddleware,
  updateMovieMiddleware,
} from "../middleware/movie.js";

import {
  createMovieController,
  deleteMovieController,
  getAllMoviesController,
  getMovieByIdController,
  updateMovieController,
} from "../controllers/movieController.js";

import { addMovieToUserListController, getAllUsersController, getUserByIdController, getUserMoviesListController } from "../controllers/userController.js";
import { createUserController, loginController } from "../controllers/auth.js";
import { tokenValidation } from "../middleware/userSessionValidation.js";
import { createUserMiddleware } from "../middleware/user.js";

const routers = Router();

// --------- auth -------------
routers.post("/login", loginController);
routers.post("/register", createUserMiddleware, createUserController);

// --------- user -------------
routers.get("/user/:id", tokenValidation, getUserByIdController);

routers.get("/user?", tokenValidation, getAllUsersController);


routers.get("/list?", tokenValidation, getUserMoviesListController);

routers.put("/list/movie/:movieId", tokenValidation, addMovieToUserListController);

// --------- movies -------------
routers.get("/movies?", tokenValidation, getAllMoviesController);

routers.get("/movies/:id", tokenValidation, getMovieByIdController);

routers.post(
  "/movies/create",
  tokenValidation,
  createMovieMiddleware,
  createMovieController
);

routers.put(
  "/movies/update",
  tokenValidation,
  updateMovieMiddleware,
  updateMovieController
);

routers.delete(
  "/movies/delete/:id",
  tokenValidation,
  deleteMovieMiddleware,
  deleteMovieController
);

export { routers };
