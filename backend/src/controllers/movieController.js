import {
  createMovieService,
  deleteMovieService,
  getAllMoviesService,
  getMovieByIdService,
  updateMovieService,
} from "../services/movieService.js";

async function createMovieController(req, res) {
  try {
    const movieData = req.body;
    const createdMovie = await createMovieService(movieData);

    return res.send(createdMovie).status(200);
  } catch (error) {
    if (error == "already_exists") {
      return res.status(400).send({
        message: "already_exists",
        code: 400,
      });
    }

    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

async function deleteMovieController(req, res) {
  try {
    const movieId = req.params.id;

    const deletedMovie = await deleteMovieService(movieId);

    return res.send(deletedMovie).status(200);
  } catch (error) {
    if (error == "already_exists") {
      return res.status(400).send({
        message: "already_exists",
        code: 400,
      });
    }

    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

async function updateMovieController(req, res) {
  try {
    const movieData = req.body;

    const updateddMovie = await updateMovieService(movieData);

    return res.send(updateddMovie).status(200);
  } catch (error) {
    if (error == "already_exists") {
      return res.status(400).send({
        message: "already_exists",
        code: 400,
      });
    }

    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

async function getAllMoviesController(req, res) {
  try {
    const { pageSize, pageOffset } = req.query;

    const movies = await getAllMoviesService({ pageSize, pageOffset });

    return res.send(movies).status(200);
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

async function getMovieByIdController(req, res) {
  try {
    const {id } = req.params;

    const movie = await getMovieByIdService({ id });

    return res.send(movie).status(200);
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

export {
  createMovieController,
  deleteMovieController,
  updateMovieController,
  getAllMoviesController,
  getMovieByIdController
};
