import {
  addMovieToUserListService,
  getAllUsersServices,
  getUserByIdService,
  getUserMoviesListService,
} from "../services/userService.js";

async function getUserByIdController(req, res) {
  try {
    const { id } = req.params;

    const user = await getUserByIdService({ id });

    return res.send(user).status(200);
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

async function getUserMoviesListController(req, res) {
  try {
    const { id } = req;
    const { pageSize, pageOffset } = req.query;

    const movies = await getUserMoviesListService({ id, pageSize, pageOffset });

    return res.send(movies).status(200);
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

async function addMovieToUserListController(req, res) {
  try {
    const { id } = req;
    const { movieId } = req.params;

    const user = await addMovieToUserListService({ movieId, userId: id });

    return res.send(user).status(200);
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

async function getAllUsersController(req, res) {
  try {
    const { pageSize, pageOffset } = req.query;
    const users = await getAllUsersServices({ pageSize, pageOffset });

    return res.send(users).status(200);
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Internal Error",
      code: 500,
    });
  }
}

export {
  getUserByIdController,
  addMovieToUserListController,
  getUserMoviesListController,
  getAllUsersController,
};
