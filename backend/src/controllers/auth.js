import { authErrors, responseMessages } from "../constants/index.js";
import { createUserService, loginService } from "../services/auth.js";

export async function loginController(req, res) {
  try {
    const userAccess = await loginService(req.body);

    return res.send(userAccess).status(200);
  } catch (error) {
    console.log(error);
    if (error == authErrors.incorrect_data) {
      return res.status(401).send(error);
    }

    if (error == authErrors.user_not_found) {
      return res.status(401).send(error);
    }

    return res.status(500).send({
      message: "Something went wrong.",
      code: 500,
    });
  }
}

export async function createUserController(req, res) {
  try {
    const userData = req.body;

    const user = await createUserService(userData);

    return res.send(user).status(200);
  } catch (error) {
    if (error == authErrors.user_already_exits) {
      return res.status(401).send(error);
    }
    console.log(error);
    return res.status(500).send(responseMessages.internal_server_error);
  }
}
