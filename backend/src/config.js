import dotenv from "dotenv";
dotenv.config();

const SECRET = process.env.SECRET;
const DATABASE_URL = process.env.DATABASE_URL;


export { SECRET, DATABASE_URL };
