export const initialMovies = [
  {
    banner: "/8YFL5QQVPy3AgrEQxNYVSgiPEbe.jpg",
    name: "Ant-Man and the Wasp: Quantumania",
    description:
      "Super-Hero partners Scott Lang and Hope van Dyne, along with with Hope's parents Janet van Dyne and Hank Pym, and Scott's daughter Cassie Lang, find themselves exploring the Quantum Realm, interacting with strange new creatures and embarking on an adventure that will push them beyond the limits of what they thought possible.",
    banner: "/ngl2FKBlU4fhbdsrtdom9LVLBXw.jpg",
    releaseDate: "2023-02-15",
    ratings: 6.5,
  },
  {
    banner: "/iJQIbOPm81fPEGKt5BPuZmfnA54.jpg",
    name: "The Super Mario Bros. Movie",
    description:
      "While working underground to fix a water main, Brooklyn plumbers—and brothers—Mario and Luigi are transported down a mysterious pipe and wander into a magical new world. But when the brothers are separated, Mario embarks on an epic quest to find Luigi.",
    banner: "/qNBAXBIQlnOThrVvA6mA2B5ggV6.jpg",
    releaseDate: "2023-04-05",
    ratings: 7.5,
  },
  {
    banner: "/nDxJJyA5giRhXx96q1sWbOUjMBI.jpg",
    name: "Shazam! Fury of the Gods",
    description:
      'Billy Batson and his foster siblings, who transform into superheroes by saying "Shazam!", are forced to get back into action and fight the Daughters of Atlas, who they must stop from using a weapon that could destroy the world.',
    banner: "/2VK4d3mqqTc7LVZLnLPeRiPaJ71.jpg",
    releaseDate: "2023-03-15",
    ratings: 6.8,
  },
  {
    banner: "/ovM06PdF3M8wvKb06i4sjW3xoww.jpg",
    name: "Avatar: The Way of Water",
    description:
      "Set more than a decade after the events of the first film, learn the story of the Sully family (Jake, Neytiri, and their kids), the trouble that follows them, the lengths they go to keep each other safe, the battles they fight to stay alive, and the tragedies they endure.",
    banner: "/t6HIqrRAclMCA60NsSmeqe9RmNV.jpg",
    releaseDate: "2022-12-14",
    ratings: 7.7,
  },
  {
    banner: "/xwA90BwZXTh8ke3CVsQlj8EOkFr.jpg",
    name: "The Last Kingdom: Seven Kings Must Die",
    description:
      "In the wake of King Edward's death, Uhtred of Bebbanburg and his comrades adventure across a fractured kingdom in the hopes of uniting England at last.",
    banner: "/7yyFEsuaLGTPul5UkHc5BhXnQ0k.jpg",
    releaseDate: "2023-04-14",
    ratings: 7.4,
  },
  {
    banner: "/5i6SjyDbDWqyun8klUuCxrlFbyw.jpg",
    name: "Creed III",
    description:
      "After dominating the boxing world, Adonis Creed has been thriving in both his career and family life. When a childhood friend and former boxing prodigy, Damian Anderson, resurfaces after serving a long sentence in prison, he is eager to prove that he deserves his shot in the ring. The face-off between former friends is more than just a fight. To settle the score, Adonis must put his future on the line to battle Damian — a fighter who has nothing to lose.",
    banner: "/cvsXj3I9Q2iyyIo95AecSd1tad7.jpg",
    releaseDate: "2023-03-01",
    ratings: 7.3,
  },
  {
    banner: "/7bWxAsNPv9CXHOhZbJVlj2KxgfP.jpg",
    name: "Evil Dead Rise",
    description:
      "Two sisters find an ancient vinyl that gives birth to bloodthirsty demons that run amok in a Los Angeles apartment building and thrusts them into a primal battle for survival as they face the most nightmarish version of family imaginable.",
    banner: "/mIBCtPvKZQlxubxKMeViO2UrP3q.jpg",
    releaseDate: "2023-04-12",
    ratings: 6.9,
  },
  {
    banner: "/bT3IpP7OopgiVuy6HCPOWLuaFAd.jpg",
    name: "Murder Mystery 2",
    description:
      "After starting their own detective agency, Nick and Audrey Spitz land a career-making case when their billionaire pal is kidnapped from his wedding.",
    banner: "/s1VzVhXlqsevi8zeCMG9A16nEUf.jpg",
    releaseDate: "2023-03-28",
    ratings: 6.6,
  },
  {
    banner: "/ouB7hwclG7QI3INoYJHaZL4vOaa.jpg",
    name: "Puss in Boots: The Last Wish",
    description:
      "Puss in Boots discovers that his passion for adventure has taken its toll: He has burned through eight of his nine lives, leaving him with only one life left. Puss sets out on an epic journey to find the mythical Last Wish and restore his nine lives.",
    banner: "/kuf6dutpsT0vSVehic3EZIqkOBt.jpg",
    releaseDate: "2022-12-07",
    ratings: 8.3,
  },
  {
    banner: "/h8gHn0OzBoaefsYseUByqsmEDMY.jpg",
    name: "John Wick: Chapter 4",
    description:
      "With the price on his head ever increasing, John Wick uncovers a path to defeating The High Table. But before he can earn his freedom, Wick must face off against a new enemy with powerful alliances across the globe and forces that turn old friends into foes.",
    banner: "/vZloFAK7NmvMGKE7VkF5UHaz0I.jpg",
    releaseDate: "2023-03-22",
    ratings: 8,
  },
  {
    banner: "/nDmPjKLqLwWyd4Ssti8HCdhW5cZ.jpg",
    name: "Adrenaline",
    description:
      "A female FBI agent holidaying in Eastern Europe with her family gets her life upside down when her daughter is kidnapped. She has to team up with a criminal on the run to save her daughter before time runs out.",
    banner: "/qVzRt8c2v4gGBYsnaflXVVeQ9Lh.jpg",
    releaseDate: "2022-12-15",
    ratings: 4,
  },
  {
    banner: "/a2tys4sD7xzVaogPntGsT1ypVoT.jpg",
    name: "Cocaine Bear",
    description:
      "Inspired by a true story, an oddball group of cops, criminals, tourists and teens converge in a Georgia forest where a 500-pound black bear goes on a murderous rampage after unintentionally ingesting cocaine.",
    banner: "/gOnmaxHo0412UVr1QM5Nekv1xPi.jpg",
    releaseDate: "2023-02-22",
    ratings: 6.4,
  },
  {
    banner: "/54IXMMEQKlkPXHqPExWy98UBmtE.jpg",
    name: "La niña de la comunión",
    description:
      "Spain, late 1980s. Newcomer Sara tries to fit in with the other teens in this tight-knit small town in the province of Tarragona. If only she were more like her extroverted best friend, Rebe. They go out one night at a nightclub, on the way home, they come upon a little girl holding a doll, dressed for her first communion. And that's when the nightmare begins.",
    banner: "/sP6AO11a7jWgsmT9T8j9EGIWAaZ.jpg",
    releaseDate: "2023-02-10",
    ratings: 6.3,
  },
  {
    banner: "/eSVu1FvGPy86TDo4hQbpuHx55DJ.jpg",
    name: "65",
    description:
      "65 million years ago, the only 2 survivors of a spaceship from Somaris that crash-landed on Earth must fend off dinosaurs and reach the escape vessel in time before an imminent asteroid strike threatens to destroy the planet.",
    banner: "/rzRb63TldOKdKydCvWJM8B6EkPM.jpg",
    releaseDate: "2023-03-02",
    ratings: 6.3,
  },
  {
    banner: "/tFaC1Fb1sv1dALB0i9Avi76MHmn.jpg",
    name: "De Piraten van Hiernaast II: De Ninja's van de Overkant",
    description:
      "The pirates feel right at home in Sandborough, but the atmosphere cools right down when the ninjas come to live in the street. After all, pirates and ninjas are sworn enemies!  While pirate captain Hector Blunderbuss struggles to get rid of his new neighbours, son Billy and ninja daughter Yuka become friends. The pirates challenge the ninjas to the ultimate battle at the village's annual hexathlon. Who will win the match? Ninjas are faster and more agile of course, but pirates are the best cheats in all of the seven seas...",
    banner: "/uDsvma9dAwnDPVuCFi99YpWvBk0.jpg",
    releaseDate: "2022-04-20",
    ratings: 6.2,
  },
  {
    banner: "/rPSJAElGxOTko1zK6uIlYnTMFxN.jpg",
    name: "Gangs of Lagos",
    description:
      "A group of friends who each have to navigate their own destiny, growing up on the bustling streets and neighborhood of Isale Eko, Lagos.",
    banner: "/nGwFsB6EXUCr21wzPgtP5juZPSv.jpg",
    releaseDate: "2023-04-07",
    ratings: 5.6,
  },
  {
    banner: "/5Y5pz0NX7SZS9036I733F7uNcwK.jpg",
    name: "The Pope's Exorcist",
    description:
      "Father Gabriele Amorth, Chief Exorcist of the Vatican, investigates a young boy's terrifying possession and ends up uncovering a centuries-old conspiracy the Vatican has desperately tried to keep hidden.",
    banner: "/9JBEPLTPSm0d1mbEcLxULjJq9Eh.jpg",
    releaseDate: "2023-04-05",
    ratings: 6.5,
  },
  {
    banner: "/m1fgGSLK0WvRpzM1AmZu38m0Tx8.jpg",
    name: "Supercell",
    description:
      "Good-hearted teenager William always lived in hope of following in his late father’s footsteps and becoming a storm chaser. His father’s legacy has now been turned into a storm-chasing tourist business, managed by the greedy and reckless Zane Rogers, who is now using William as the main attraction to lead a group of unsuspecting adventurers deep into the eye of the most dangerous supercell ever seen.",
    banner: "/gbGHezV6yrhua0KfAgwrknSOiIY.jpg",
    releaseDate: "2023-03-17",
    ratings: 6.4,
  },
  {
    banner: "/tYcmm8XtzRdcT6kliCbHuWwLCwB.jpg",
    name: "길복순",
    description:
      "At work, she's a renowned assassin. At home, she's a single mom to a teenage daughter. Killing? That's easy. It's parenting that's the hard part.",
    banner: "/taYgn3RRpCGlTGdaGQvnSIOzXFy.jpg",
    releaseDate: "2023-02-17",
    ratings: 6.8,
  },
  {
    banner: "/eNJhWy7xFzR74SYaSJHqJZuroDm.jpg",
    name: "Attack on Titan",
    description:
      "As viable water is depleted on Earth, a mission is sent to Saturn's moon Titan to retrieve sustainable H2O reserves from its alien inhabitants. But just as the humans acquire the precious resource, they are attacked by Titan rebels, who don't trust that the Earthlings will leave in peace.",
    banner: "/qNz4l8UgTkD8rlqiKZ556pCJ9iO.jpg",
    releaseDate: "2022-09-30",
    ratings: 6.1,
  },
  {
    banner: "/fY3lD0jM5AoHJMunjGWqJ0hRteI.jpg",
    description:
      "In postwar Japan, Godzilla brings new devastation to an already scorched landscape. With no military intervention or government help in sight, the survivors must join together in the face of despair and fight back against an unrelenting horror.",
    name: "ゴジラ-1.0",
    ratings: 7.616,
    releaseDate: "2023-11-03",
  },
  {
    banner: "/iafs5DG5fGq7ef0acl3xlX4BFrs.jpg",
    description: "",
    name: "Un père idéal",
    ratings: 5.872,
    releaseDate: "2024-04-21",
  },
  {
    banner: "/xOMo8BRK7PfcJv9JCnx7s5hj0PX.jpg",
    description:
      "Follow the mythic journey of Paul Atreides as he unites with Chani and the Fremen while on a path of revenge against the conspirators who destroyed his family. Facing a choice between the love of his life and the fate of the known universe, Paul endeavors to prevent a terrible future only he can foresee.",
    name: "Dune: Part Two",
    ratings: 8.167,
    releaseDate: "2024-02-27",
  },
  {
    banner: "/s9hW1DHfgy5ppK1fTUJuMKh4YFK.jpg",
    description:
      "An airliner filled with 800 passengers is forced to fly fast and low, above farmlands, suburbs and skyscraper-packed cities or the tons of explosives aboard will detonate. When an elite unit of US Air Force fighter jets is sent to provide escort, they find themselves facing a squadron of unidentifiable warplanes which ignites a deadly air battle that threatens to destroy all life above and below.",
    name: "Top Gunner: Danger Zone",
    ratings: 4.632,
    releaseDate: "2022-05-20",
  },
  {
    banner: "/9az3E8loGzTYazvLvoEWI7Flu3M.jpg",
    description:
      "Lisa, aka Kali, who has nothing left to lose when she finds out her husband has been executed in Rio to cover up a corruption scandal. A former Special Forces recruit, she heads to Brazil with fists, blood and explosives, to Know the truth",
    name: "KALI : L'ange de la Vengeance",
    ratings: 7.013,
    releaseDate: "2024-05-31",
  },
  {
    banner: "/vWzJDjLPmycnQ42IppEjMpIhrhc.jpg",
    description:
      "Garfield, the world-famous, Monday-hating, lasagna-loving indoor cat, is about to have a wild outdoor adventure! After an unexpected reunion with his long-lost father – scruffy street cat Vic – Garfield and his canine friend Odie are forced from their perfectly pampered life into joining Vic in a hilarious, high-stakes heist.",
    name: "The Garfield Movie",
    ratings: 6.43,
    releaseDate: "2024-04-30",
  },
  {
    banner: "/b93N6Mb08NhFhobB8KrR5GBaPP5.jpg",
    description:
      "A small team of scientists must race against time to stop what seems to be a cascade of global disasters signaling the possible apocalypse and end of days.",
    name: "4 Horsemen: Apocalypse",
    ratings: 5.876,
    releaseDate: "2022-04-29",
  },
  {
    banner: "/5Eip60UDiPLASyKjmHPMruggTc4.jpg",
    description:
      "An American nun embarks on a new journey when she joins a remote convent in the Italian countryside. However, her warm welcome quickly turns into a living nightmare when she discovers her new home harbours a sinister secret and unspeakable horrors.",
    name: "Immaculate",
    ratings: 6.3,
    releaseDate: "2024-03-20",
  },
  {
    banner: "/1m1rXopfNDVL3UMiv6kriYaJ3yE.jpg",
    description:
      "When his family is murdered, a deaf-mute named Boy escapes to the jungle and is trained by a mysterious shaman to repress his childish imagination and become an instrument of death.",
    name: "Boy Kills World",
    ratings: 6.909,
    releaseDate: "2024-04-24",
  },
  {
    banner: "/r4TxCaZvQ2bLFoXRLHGfii6b3tJ.jpg",
    description:
      "Marcus Burnett is a henpecked family man. Mike Lowry is a footloose and fancy free ladies' man. Both Miami policemen, they have 72 hours to reclaim a consignment of drugs stolen from under their station's nose. To complicate matters, in order to get the assistance of the sole witness to a murder, they have to pretend to be each other.",
    name: "Bad Boys",
    ratings: 6.799,
    releaseDate: "1995-04-07",
  },
  {
    banner: "/lntyt4OVDbcxA1l7LtwITbrD3FI.jpg",
    description:
      "Noah must leave her city, boyfriend, and friends to move into William Leister's mansion, the flashy and wealthy husband of her mother Rafaela. As a proud and independent 17 year old, Noah resists living in a mansion surrounded by luxury. However, it is there where she meets Nick, her new stepbrother, and the clash of their strong personalities becomes evident from the very beginning.",
    name: "Culpa mía",
    ratings: 7.964,
    releaseDate: "2023-06-08",
  },
  {
    banner: "/4woSOUD0equAYzvwhWBHIJDCM88.jpg",
    description:
      "Characters from different backgrounds are thrown together when the plane they're travelling on crashes into the Pacific Ocean. A nightmare fight for survival ensues with the air supply running out and dangers creeping in from all sides.",
    name: "No Way Up",
    ratings: 6.46,
    releaseDate: "2024-01-18",
  },
  {
    banner: "/rzu9BVI9HqY5rG0Xq05ZhewdkYP.jpg",
    description:
      "Lucy and Jane have been best friends for most of their lives and think they know everything there is to know about each other. But when Jane announces she's moving to London, Lucy reveals a long-held secret. As Jane tries to help Lucy, their friendship is thrown into chaos.",
    name: "Am I OK?",
    ratings: 6.089,
    releaseDate: "2024-06-11",
  },
  {
    banner: "/gxSVZCUlDd8upT1G2wdrUdz2hxG.jpg",
    description:
      "Detectives Marcus Burnett and Mike Lowrey of the Miami Narcotics Task Force are tasked with stopping the flow of the drug ecstasy into Miami. They track the drugs to the whacked-out Cuban drug lord Johnny Tapia, who is also involved in a bloody war with Russian and Haitian mobsters. If that isn't bad enough, there's tension between the two detectives when Marcus discovers that playboy Mike is secretly romancing Marcus’ sister, Syd.",
    name: "Bad Boys II",
    ratings: 6.617,
    releaseDate: "2003-07-18",
  },
  {
    banner: "/ksZTspehDCC4PCZN8ci31pFT6yp.jpg",
    description:
      "The Imjin War reaches its seventh year in December of 1598. Admiral Yi Sun-shin learns that the Wa invaders in Joseon are preparing for a swift withdrawal following the deathbed orders of their leader Toyotomi Hideyoshi. Determined to destroy the enemy once and for all, Admiral Yi leads an allied fleet of Joseon and Ming ships to mount a blockade and annihilate the Wa army. However, once Ming commander Chen Lin is bribed into lifting the blockade, Wa lord Shimazu Yoshihiro and his Satsuma army sail to the Wa army’s rescue at Noryang Strait.",
    name: "노량: 죽음의 바다",
    ratings: 6.197,
    releaseDate: "2023-12-20",
  },
  {
    banner: "/sI6uCeF8mUlZx22mFfHSi9W3XQ9.jpg",
    description:
      "Solène, a 40-year-old single mom, begins an unexpected romance with 24-year-old Hayes Campbell, the lead singer of August Moon, the hottest boy band on the planet. When Solène must step in to chaperone her teenage daughter's trip to the Coachella Music Festival after her ex bails at the last minute, she has a chance encounter with Hayes and there is an instant, undeniable spark. As they begin a whirlwind romance, it isn't long before Hayes' superstar status poses unavoidable challenges to their relationship, and Solène soon discovers that life in the glare of his spotlight might be more than she bargained for.",
    name: "The Idea of You",
    ratings: 7.409,
    releaseDate: "2024-05-02",
  },
  {
    banner: "/hU1Q9YVzdYhokr8a9gLywnSUMlN.jpg",
    description:
      "Miraculous holders from another world appear in Paris. They come from a parallel universe where everything is reversed: the holders of Ladybug and Black Cat Miraculouses, Shadybug and Claw Noir, are the bad guys, and the holder of the Butterfly Miraculous, Hesperia, is a superhero. Ladybug and Cat Noir will have to help Hesperia counter the attacks of their evil doubles and prevent them from seizing the Butterfly's Miraculous. Can our heroes also help Hesperia make Shadybug and Claw Noir better people?",
    name: "Miraculous World : Paris, Les Aventures de Toxinelle et Griffe Noire",
    ratings: 7.313,
    releaseDate: "2023-10-21",
  },
  {
    banner: "/2aBDIqdSchYcPnkXqMRctRfYjSV.jpg",
    description:
      "In a desperate bid to secure a future for her child, an undocumented immigrant mother takes a caretaker job. Unbeknownst to her, the elderly man conceals a horrifying truth.",
    name: "Silence of the Prey",
    ratings: 3.8,
    releaseDate: "2024-05-14",
  },
  {
    banner: "/mYIWIFZRrxCVsOUFM9uUpOdgJ3I.jpg",
    description:
      "Two young children and their parents help a trio of aliens transformed as friendly dogs to escape the clutches of a local UFO hunter as they repair their spaceship.",
    name: "Space Pups",
    ratings: 6,
    releaseDate: "2023-07-27",
  },
  {
    banner: "/rZ8VxBH8QRHGQi9YztBRm3eAsxL.jpg",
    description:
      "Mindy adopts Angel, a high-spirited pony that—according to legend—will lead its owner to gold hidden in the nearby mountains. When Angel is kidnapped by a mad treasure hunter, Mindy and her school friends head into the hills to rescue the pony and hunt for the lost gold. But Mindy meets a mountain man that warns her the treasure is part of an ancient curse—if they remove the gold, they'll destroy the beautiful forest.",
    name: "The Legend of Catclaws Mountain",
    ratings: 4.667,
    releaseDate: "2024-05-27",
  },
  {
    banner: "/fY3lD0jM5AoHJMunjGWqJ0hRteI.jpg",
    description:
      "In postwar Japan, Godzilla brings new devastation to an already scorched landscape. With no military intervention or government help in sight, the survivors must join together in the face of despair and fight back against an unrelenting horror.",
    name: "ゴジラ-1.0",
    ratings: 7.616,
    releaseDate: "2023-11-03",
  },
  {
    banner: "/iafs5DG5fGq7ef0acl3xlX4BFrs.jpg",
    description: "",
    name: "Un père idéal",
    ratings: 5.872,
    releaseDate: "2024-04-21",
  },
  {
    banner: "/xOMo8BRK7PfcJv9JCnx7s5hj0PX.jpg",
    description:
      "Follow the mythic journey of Paul Atreides as he unites with Chani and the Fremen while on a path of revenge against the conspirators who destroyed his family. Facing a choice between the love of his life and the fate of the known universe, Paul endeavors to prevent a terrible future only he can foresee.",
    name: "Dune: Part Two",
    ratings: 8.167,
    releaseDate: "2024-02-27",
  },
  {
    banner: "/s9hW1DHfgy5ppK1fTUJuMKh4YFK.jpg",
    description:
      "An airliner filled with 800 passengers is forced to fly fast and low, above farmlands, suburbs and skyscraper-packed cities or the tons of explosives aboard will detonate. When an elite unit of US Air Force fighter jets is sent to provide escort, they find themselves facing a squadron of unidentifiable warplanes which ignites a deadly air battle that threatens to destroy all life above and below.",
    name: "Top Gunner: Danger Zone",
    ratings: 4.632,
    releaseDate: "2022-05-20",
  },
  {
    banner: "/9az3E8loGzTYazvLvoEWI7Flu3M.jpg",
    description:
      "Lisa, aka Kali, who has nothing left to lose when she finds out her husband has been executed in Rio to cover up a corruption scandal. A former Special Forces recruit, she heads to Brazil with fists, blood and explosives, to Know the truth",
    name: "KALI : L'ange de la Vengeance",
    ratings: 7.013,
    releaseDate: "2024-05-31",
  },
  {
    banner: "/vWzJDjLPmycnQ42IppEjMpIhrhc.jpg",
    description:
      "Garfield, the world-famous, Monday-hating, lasagna-loving indoor cat, is about to have a wild outdoor adventure! After an unexpected reunion with his long-lost father – scruffy street cat Vic – Garfield and his canine friend Odie are forced from their perfectly pampered life into joining Vic in a hilarious, high-stakes heist.",
    name: "The Garfield Movie",
    ratings: 6.43,
    releaseDate: "2024-04-30",
  },
  {
    banner: "/b93N6Mb08NhFhobB8KrR5GBaPP5.jpg",
    description:
      "A small team of scientists must race against time to stop what seems to be a cascade of global disasters signaling the possible apocalypse and end of days.",
    name: "4 Horsemen: Apocalypse",
    ratings: 5.876,
    releaseDate: "2022-04-29",
  },
  {
    banner: "/5Eip60UDiPLASyKjmHPMruggTc4.jpg",
    description:
      "An American nun embarks on a new journey when she joins a remote convent in the Italian countryside. However, her warm welcome quickly turns into a living nightmare when she discovers her new home harbours a sinister secret and unspeakable horrors.",
    name: "Immaculate",
    ratings: 6.3,
    releaseDate: "2024-03-20",
  },
  {
    banner: "/1m1rXopfNDVL3UMiv6kriYaJ3yE.jpg",
    description:
      "When his family is murdered, a deaf-mute named Boy escapes to the jungle and is trained by a mysterious shaman to repress his childish imagination and become an instrument of death.",
    name: "Boy Kills World",
    ratings: 6.909,
    releaseDate: "2024-04-24",
  },
  {
    banner: "/r4TxCaZvQ2bLFoXRLHGfii6b3tJ.jpg",
    description:
      "Marcus Burnett is a henpecked family man. Mike Lowry is a footloose and fancy free ladies' man. Both Miami policemen, they have 72 hours to reclaim a consignment of drugs stolen from under their station's nose. To complicate matters, in order to get the assistance of the sole witness to a murder, they have to pretend to be each other.",
    name: "Bad Boys",
    ratings: 6.799,
    releaseDate: "1995-04-07",
  },
  {
    banner: "/lntyt4OVDbcxA1l7LtwITbrD3FI.jpg",
    description:
      "Noah must leave her city, boyfriend, and friends to move into William Leister's mansion, the flashy and wealthy husband of her mother Rafaela. As a proud and independent 17 year old, Noah resists living in a mansion surrounded by luxury. However, it is there where she meets Nick, her new stepbrother, and the clash of their strong personalities becomes evident from the very beginning.",
    name: "Culpa mía",
    ratings: 7.964,
    releaseDate: "2023-06-08",
  },
  {
    banner: "/4woSOUD0equAYzvwhWBHIJDCM88.jpg",
    description:
      "Characters from different backgrounds are thrown together when the plane they're travelling on crashes into the Pacific Ocean. A nightmare fight for survival ensues with the air supply running out and dangers creeping in from all sides.",
    name: "No Way Up",
    ratings: 6.46,
    releaseDate: "2024-01-18",
  },
  {
    banner: "/rzu9BVI9HqY5rG0Xq05ZhewdkYP.jpg",
    description:
      "Lucy and Jane have been best friends for most of their lives and think they know everything there is to know about each other. But when Jane announces she's moving to London, Lucy reveals a long-held secret. As Jane tries to help Lucy, their friendship is thrown into chaos.",
    name: "Am I OK?",
    ratings: 6.089,
    releaseDate: "2024-06-11",
  },
  {
    banner: "/gxSVZCUlDd8upT1G2wdrUdz2hxG.jpg",
    description:
      "Detectives Marcus Burnett and Mike Lowrey of the Miami Narcotics Task Force are tasked with stopping the flow of the drug ecstasy into Miami. They track the drugs to the whacked-out Cuban drug lord Johnny Tapia, who is also involved in a bloody war with Russian and Haitian mobsters. If that isn't bad enough, there's tension between the two detectives when Marcus discovers that playboy Mike is secretly romancing Marcus’ sister, Syd.",
    name: "Bad Boys II",
    ratings: 6.617,
    releaseDate: "2003-07-18",
  },
  {
    banner: "/ksZTspehDCC4PCZN8ci31pFT6yp.jpg",
    description:
      "The Imjin War reaches its seventh year in December of 1598. Admiral Yi Sun-shin learns that the Wa invaders in Joseon are preparing for a swift withdrawal following the deathbed orders of their leader Toyotomi Hideyoshi. Determined to destroy the enemy once and for all, Admiral Yi leads an allied fleet of Joseon and Ming ships to mount a blockade and annihilate the Wa army. However, once Ming commander Chen Lin is bribed into lifting the blockade, Wa lord Shimazu Yoshihiro and his Satsuma army sail to the Wa army’s rescue at Noryang Strait.",
    name: "노량: 죽음의 바다",
    ratings: 6.197,
    releaseDate: "2023-12-20",
  },
  {
    banner: "/sI6uCeF8mUlZx22mFfHSi9W3XQ9.jpg",
    description:
      "Solène, a 40-year-old single mom, begins an unexpected romance with 24-year-old Hayes Campbell, the lead singer of August Moon, the hottest boy band on the planet. When Solène must step in to chaperone her teenage daughter's trip to the Coachella Music Festival after her ex bails at the last minute, she has a chance encounter with Hayes and there is an instant, undeniable spark. As they begin a whirlwind romance, it isn't long before Hayes' superstar status poses unavoidable challenges to their relationship, and Solène soon discovers that life in the glare of his spotlight might be more than she bargained for.",
    name: "The Idea of You",
    ratings: 7.409,
    releaseDate: "2024-05-02",
  },
  {
    banner: "/hU1Q9YVzdYhokr8a9gLywnSUMlN.jpg",
    description:
      "Miraculous holders from another world appear in Paris. They come from a parallel universe where everything is reversed: the holders of Ladybug and Black Cat Miraculouses, Shadybug and Claw Noir, are the bad guys, and the holder of the Butterfly Miraculous, Hesperia, is a superhero. Ladybug and Cat Noir will have to help Hesperia counter the attacks of their evil doubles and prevent them from seizing the Butterfly's Miraculous. Can our heroes also help Hesperia make Shadybug and Claw Noir better people?",
    name: "Miraculous World : Paris, Les Aventures de Toxinelle et Griffe Noire",
    ratings: 7.313,
    releaseDate: "2023-10-21",
  },
  {
    banner: "/2aBDIqdSchYcPnkXqMRctRfYjSV.jpg",
    description:
      "In a desperate bid to secure a future for her child, an undocumented immigrant mother takes a caretaker job. Unbeknownst to her, the elderly man conceals a horrifying truth.",
    name: "Silence of the Prey",
    ratings: 3.8,
    releaseDate: "2024-05-14",
  },
  {
    banner: "/mYIWIFZRrxCVsOUFM9uUpOdgJ3I.jpg",
    description:
      "Two young children and their parents help a trio of aliens transformed as friendly dogs to escape the clutches of a local UFO hunter as they repair their spaceship.",
    name: "Space Pups",
    ratings: 6,
    releaseDate: "2023-07-27",
  },
  {
    banner: "/rZ8VxBH8QRHGQi9YztBRm3eAsxL.jpg",
    description:
      "Mindy adopts Angel, a high-spirited pony that—according to legend—will lead its owner to gold hidden in the nearby mountains. When Angel is kidnapped by a mad treasure hunter, Mindy and her school friends head into the hills to rescue the pony and hunt for the lost gold. But Mindy meets a mountain man that warns her the treasure is part of an ancient curse—if they remove the gold, they'll destroy the beautiful forest.",
    name: "The Legend of Catclaws Mountain",
    ratings: 4.667,
    releaseDate: "2024-05-27",
  },

  {
    banner: "/qwK9soQmmJ7kRdjLZVXblw3g7AQ.jpg",
    description:
      "Xander Cage is your standard adrenaline junkie with no fear and a lousy attitude. When the US Government &quot;recruits&quot; him to go on a mission, he's not exactly thrilled. His mission: to gather information on an organization that may just be planning the destruction of the world, led by the nihilistic Yorgi.",
    name: "xXx",
    ratings: 5.931,
    releaseDate: "2002-08-09",
  },
  {
    banner: "/hvqNAz3cq48sh9GKxu4lPiogfBo.jpg",
    description:
      "An ex-special forces operative wages a one man war through the streets of Amsterdam to rescue his friend's daughter from the local crime syndicate.",
    name: "Black Lotus",
    ratings: 6.449,
    releaseDate: "2023-04-12",
  },
  {
    banner: "/7ZP8HtgOIDaBs12krXgUIygqEsy.jpg",
    description:
      "Gurus in the late Goryeo dynasty try to obtain a fabled, holy sword, and humans in 2022 hunt down an alien prisoner that is locked in a human's body. The two parties cross paths when a time-traveling portal opens up.",
    name: "외계+인 1부",
    ratings: 6.921,
    releaseDate: "2022-07-20",
  },
  {
    banner: "/qekky2LbtT1wtbD5MDgQvjfZQ24.jpg",
    description:
      "When an explosion at an oil well threatens hundreds of lives, a crack team is called upon to make a deadly desert crossing with nitroglycerine in tow.",
    name: "Le salaire de la peur",
    ratings: 5.953,
    releaseDate: "2024-03-28",
  },
  {
    banner: "/oe7mWkvYhK4PLRNAVSvonzyUXNy.jpg",
    description:
      "Ex-UFC fighter Dalton takes a job as a bouncer at a Florida Keys roadhouse, only to discover that this paradise is not all it seems.",
    name: "Road House",
    ratings: 7.015,
    releaseDate: "2024-03-08",
  },
  {
    banner: "/jnE1GA7cGEfv5DJBoU2t4bZHaP4.jpg",
    description:
      "Recognizing the destructive power of its captive giant Ape, the military makes its own battle-ready A.I., Mecha Ape. But its first practical test goes horribly wrong, leaving the military no choice but to release the imprisoned giant ape to stop the colossal robot before it destroys downtown Chicago.",
    name: "Ape vs Mecha Ape",
    ratings: 5.723,
    releaseDate: "2023-03-24",
  },
  {
    banner: "/2KGxQFV9Wp1MshPBf8BuqWUgVAz.jpg",
    description:
      "After a migrating duck family alights on their pond with thrilling tales of far-flung places, the Mallard family embarks on a family road trip, from New England, to New York City, to tropical Jamaica.",
    name: "Migration",
    ratings: 7.442,
    releaseDate: "2023-12-06",
  },
  {
    banner: "/oZDRuGHhe5uY8wBqFJcJZT9kdvJ.jpg",
    description:
      "When 28-year-old artist Mina finds shelter after getting stranded in an expansive, untouched forest in western Ireland, she unknowingly becomes trapped alongside three strangers that are watched and stalked by mysterious creatures each night.",
    name: "The Watchers",
    ratings: 5.9,
    releaseDate: "2024-06-06",
  },
  {
    banner: "/hzr7GhCoWTeQQMKDR6LXhN8x9fs.jpg",
    description:
      "A listless Wade Wilson toils away in civilian life with his days as the morally flexible mercenary, Deadpool, behind him. But when his homeworld faces an existential threat, Wade must reluctantly suit-up again with an even more reluctant Wolverine.",
    name: "Deadpool &amp; Wolverine",
    ratings: 0,
    releaseDate: "2024-07-24",
  },
  {
    banner: "/tpiqEVTLRz2Mq7eLq5DT8jSrp71.jpg",
    description:
      "The rebels gear up for battle against the ruthless forces of the Motherworld as unbreakable bonds are forged, heroes emerge — and legends are made.",
    name: "Rebel Moon - Part Two: The Scargiver",
    ratings: 6.1,
    releaseDate: "2024-04-19",
  },
  {
    banner: "/wODqakS0jinTUECNS6n4VomQbew.jpg",
    description:
      "When the discovery of an ancient artifact unleashes an evil force, Ghostbusters new and old must join forces to protect their home and save the world from a second Ice Age.",
    name: "Ghostbusters: Frozen Empire",
    ratings: 6.652,
    releaseDate: "2024-03-20",
  },
  {
    banner: "/7kQvHmGyKJv2wSKVfCUux50rd7A.jpg",
    description:
      "The soldier king Qin Yang's fiancée Ye Qin met with an unknown beast and died tragically. Gu Ping invites him to participate in Ye Qin's scientific research before her death. But Gu Ping is using Ye Qin's research results to combine the genes of unknown beasts to create the &quot;Zero&quot; dragon creature. The intelligent dragon creature, coupled with the extra-terrestrial beast evolved by devouring,  an imminent city war is coming...",
    name: "异兽战场",
    ratings: 6.744,
    releaseDate: "2021-12-27",
  },
  {
    banner: "/rmNlWyez5cniGtXkgixG1ezdqVk.jpg",
    description:
      "After learning that the death of his wife was not an accident, a former CIA Station Chief is forced back into the espionage underworld, teaming up with an adversary to unravel a conspiracy that challenges everything he thought he knew.",
    name: "Chief of Station",
    ratings: 5.272,
    releaseDate: "2024-05-02",
  },
  {
    banner: "/k37Ccgu05Am1xxgN5GaW0HX9Kkl.jpg",
    description:
      "After raising an unnervingly talented spider in secret, 12-year-old Charlotte must face the truth about her pet and fight for her family's survival.",
    name: "Sting",
    ratings: 6.419,
    releaseDate: "2024-04-12",
  },
  {
    banner: "/AuKXTyCVzeIdtTe1NAQnJa1ZC7I.jpg",
    description:
      "Thelma dreams of being a glamorous unicorn. Then in a rare pink and glitter-filled moment of fate, Thelma's wish comes true. She rises to instant international stardom, but at an unexpected cost. After a while, Thelma realizes that she was happier as her ordinary, sparkle-free self. So she ditches her horn, scrubs off her sparkles, and returns home, where her best friend is waiting for her with a hug.",
    name: "Thelma the Unicorn",
    ratings: 6.9,
    releaseDate: "2024-05-17",
  },
  {
    banner: "/ySgY4jBvZ6qchrxKnBg4M8tZp8V.jpg",
    description:
      "A group of criminals kidnaps a teenage ballet dancer, the daughter of a notorious gang leader, in order to obtain a ransom of $50 million, but over time, they discover that she is not just an ordinary girl. After the kidnappers begin to diminish, one by one, they discover, to their increasing horror, that they are locked inside with an unusual girl.",
    name: "Abigail",
    ratings: 6.815,
    releaseDate: "2024-04-18",
  },
  {
    banner: "/pwGmXVKUgKN13psUjlhC9zBcq1o.jpg",
    description:
      "Forced to confront revelations about her past, paramedic Cassandra Webb forges a relationship with three young women destined for powerful futures...if they can all survive a deadly present.",
    name: "Madame Web",
    ratings: 5.61,
    releaseDate: "2024-02-14",
  },
  {
    banner: "/7KcwyOQL6hC6VJxN7LBB1p2GRaY.jpg",
    description:
      "Criminals control Mexico and wrestling is now illegal. A retired fighter and a policewoman join forces to stop the perfidious criminal who has kidnapped her son.",
    name: "El Halcón: Sed de Venganza",
    ratings: 8.222,
    releaseDate: "2023-10-25",
  },
  {
    banner: "/lA93AaUEv6VYRvzpy0XMOKYwEoN.jpg",
    description: "Sequel to The 100 Candles Game (2020).",
    name: "The 100 Candles Game: The Last Possession",
    ratings: 5.8,
    releaseDate: "2023-11-09",
  },
  {
    banner: "/nxxCPRGTzxUH8SFMrIsvMmdxHti.jpg",
    description:
      "A young girl who goes through a difficult experience begins to see everyone's imaginary friends who have been left behind as their real-life friends have grown up.",
    name: "IF",
    ratings: 7.05,
    releaseDate: "2024-05-08",
  },
  {
    banner: "/4XM8DUTQb3lhLemJC51Jx4a2EuA.jpg",
    description: "Over many missions and against impossible odds, Dom Toretto and his family have outsmarted, out-nerved and outdriven every foe in their path. Now, they confront the most lethal opponent they've ever faced: A terrifying threat emerging from the shadows of the past who's fueled by blood revenge, and who is determined to shatter this family and destroy everything—and everyone—that Dom loves, forever.",
    name: "Fast X",
    ratings: 7.118,
    releaseDate: "2023-05-17"
  }, {
    banner: "/9wJO4MBzkqgUZemLTGEsgUbYyP6.jpg",
    description: "Air-supply is scarce in the near future, forcing a mother and daughter to fight for survival when two strangers arrive desperate for an oxygenated haven.",
    name: "Breathe",
    ratings: 5.112,
    releaseDate: "2024-04-04"
  }, {
    banner: "/9Or1qeUZyOSQVglRXGnqc6owp21.jpg",
    description: "When Karate champion Michael Rivers wins the last tournament of his career, shady businessman Ron Hall offers him the opportunity to fight in an illegal Kumite in Bulgaria against the world’s best martial artists.  When Michael declines, Hall has his daughter kidnapped and, in order to rescue her, Rivers is left with no choice but to compete in the deadly tournament. Arriving in Bulgaria, he finds out that he is not the only fighter whose loved one was taken. Rivers enlists the help of trainers Master Loren, and Julie Jackson but will it be enough for him to win the tournament and save his daughter’s life?",
    name: "The Last Kumite",
    ratings: 4.7,
    releaseDate: "2024-05-09"
  }, {
    banner: "/mYPsDej53aibXId6BSxb3Q5fAHg.jpg",
    description: "Roy Freeman, an ex-homicide detective with a fractured memory, is forced to revisit a case he can't remember. As a man's life hangs in the balance on death row, Freeman must piece together the brutal evidence from a decade-old murder investigation, uncovering a sinister web of buried secrets and betrayals linking to his past. With only instincts to trust, he faces a chilling truth - sometimes, it's best to let sleeping dogs lie.",
    name: "Sleeping Dogs",
    ratings: 7.174,
    releaseDate: "2024-03-21"
  }, {
    banner: "/iy08wTsqcWYT2PFTEWFYcxepLeB.jpg",
    description: "A number of children are being well fed, taken care of, and pampered at a very meticulate and managed orphanage. The facility and grounds are impressive, but the wall acting as a barrier is high. There is a secret to the place and once revealed to several orphans, they're desperate to escape.",
    name: "約束のネバーランド",
    ratings: 7.427,
    releaseDate: "2020-12-18"
  }, {
    banner: "/x1Ex090BNIXcmaBYB2US0j3hBUF.jpg",
    description: "Han, a Filipino-Korean(Kopino) boxer hits the final punch against his opponent at an illegal game. The opponent is knocked down, and Han returns home with a few dollars to pay for his mother’s surgery. Although he is aware that he shouldn't, he strives to find his father, whom he saw in an old picture. Then, one day, Han rushes to the Kopino Support Center after receiving an unexpected phone call from Mr. Kim, and he follows those who came to pick him up on a board to Korea. Meanwhile, a mysterious child appears and a man keeps an eye on Han at the airport. Who or what is chasing him?",
    name: "The Childe",
    ratings: 7.1,
    releaseDate: "2023-06-21"
  }, {
    banner: "/kGWpaisyiOrOhkjn5FviMRUaoCb.jpg",
    description: "This is the story of a puppy that touched hundreds of millions of people around the world. Hachiko is a cute Chinese pastoral dog. He met his destined owner Chen Jingxiu in the vast crowd and became a member of the Chen family. With the passage of time, the once beautiful home is no longer there, but Batong is still waiting where it is, and its fate is closely tied to its family.",
    name: "忠犬八公",
    ratings: 7.7,
    releaseDate: "2023-03-31"
  }, {
    banner: "/7IgL9NzlXSSHXyYoZHXVJEKQhkg.jpg",
    description: "It's Mother's Day and Marge Simpson joins the moms of Disney+ on a special holiday outing that turns into an epic galactic adventure filled with heroes, villains, and Stewie from Family Guy.",
    name: "May the 12th Be with You",
    ratings: 5.125,
    releaseDate: "2024-05-09"
  }, {
    banner: "/lUlz0oE3zhAku7QvGoDJCeaFuiO.jpg",
    description: "An ex-military doctor finds herself in a deadly battle for survival when the Irish mafia seize control of the hospital at which she works. When her son is taken hostage, she is forced to rely upon her battle-hardened past and lethal skills after realizing there’s no one left to save the day but her.",
    name: "Mercy",
    ratings: 6.2,
    releaseDate: "2023-05-12"
  }, {
    banner: "/fUC5VsQcU3m6zmYMD96R7RqPuMn.jpg",
    description: "A Chicago detective travels to Scotland after an emerging serial killer’s crimes match those that he investigated five years earlier, one of which was the crime scene of his murdered girlfriend.",
    name: "Damaged",
    ratings: 5.5,
    releaseDate: "2024-04-12"
  }, {
    banner: "/504GSaoxBA5nLlcMsJfjzLZEKUp.jpg",
    description: "Tennis player turned coach Tashi has taken her husband, Art, and transformed him into a world-famous Grand Slam champion. To jolt him out of his recent losing streak, she signs him up for a \&quot;Challenger\&quot; event — close to the lowest level of pro tournament — where he finds himself standing across the net from his former best friend and Tashi's former boyfriend.",
    name: "Challengers",
    ratings: 7.239,
    releaseDate: "2024-04-18"
  }, {
    banner: "/91wcqNBLHhKWBIhMknG4qa6ejxE.jpg",
    description: "D'Artagnan, on a quest to rescue the abducted Constance, runs into the mysterious Milady de Winter again. The tension between the Catholics and the Protestants finally escalates, as the king declares war — forcing the now four musketeers into battle. But as the war goes on, they are tested physically, mentally and emotionally.",
    name: "Les trois mousquetaires : Milady",
    ratings: 6.477,
    releaseDate: "2023-12-13"
  }, {
    banner: "/9c0lHTXRqDBxeOToVzRu0GArSne.jpg",
    description: "Set in a post-apocalyptic world where a global airborne pandemic has wiped out 90% of the Earth's population and only the young and immune have endured as scavengers. For Ellie and Quinn, the daily challenges to stay alive are compounded when they become hunted by the merciless Stalkers.",
    name: "After the Pandemic",
    ratings: 4.794,
    releaseDate: "2022-03-01"
  }, {
    banner: "/nb3xI8XI3w4pMVZ38VijbsyBqP4.jpg",
    description: "The story of J. Robert Oppenheimer's role in the development of the atomic bomb during World War II.",
    name: "Oppenheimer",
    ratings: 8.094,
    releaseDate: "2023-07-19"
  }, {
    banner: "/ehumsuIBbgAe1hg343oszCLrAfI.jpg",
    description: "Asha, a sharp-witted idealist, makes a wish so powerful that it is answered by a cosmic force – a little ball of boundless energy called Star. Together, Asha and Star confront a most formidable foe - the ruler of Rosas, King Magnifico - to save her community and prove that when the will of one courageous human connects with the magic of the stars, wondrous things can happen.",
    name: "Wish",
    ratings: 6.425,
    releaseDate: "2023-11-13"
  }, {
    banner: "/ycCj6Ssuu2IdM23AYR7B8nbxQPA.jpg",
    description: "Gru and Lucy and their girls — Margo, Edith and Agnes — welcome a new member to the Gru family, Gru Jr., who is intent on tormenting his dad. Gru faces a new nemesis in Maxime Le Mal and his femme fatale girlfriend Valentina, and the family is forced to go on the run.",
    name: "Despicable Me 4",
    ratings: 0,
    releaseDate: "2024-06-20"
  }, {
    banner: "/tDmAhKMBv0e4sYyg4FX5LVfe64S.jpg",
    description: "After a deadly forest fire sweeps the earth and fills the air with pollutants, society is on the brink of collapse and riddled with disease. The only hope seems to be in a legend come to life: that of the Slyth, a reptilian species that can disguise themselves as humans and possesses blood that can cure disease. The hunt is on.",
    name: "สลิธ โปรเจกต์ล่า",
    ratings: 4.417,
    releaseDate: "2023-12-05"
  }, {
    banner: "/mDfJG3LC3Dqb67AZ52x3Z0jU0uB.jpg",
    description: "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
    name: "Avengers: Infinity War",
    ratings: 8.246,
    releaseDate: "2018-04-25"
  }, {
    banner: "/yyFc8Iclt2jxPmLztbP617xXllT.jpg",
    description: "Willy Wonka – chock-full of ideas and determined to change the world one delectable bite at a time – is proof that the best things in life begin with a dream, and if you’re lucky enough to meet Willy Wonka, anything is possible.",
    name: "Wonka",
    ratings: 7.167,
    releaseDate: "2023-12-06"
  }, {
    banner: "/8yACFuo4OaIiKr9hHFlmPcGalKx.jpg",
    description: "An apocalyptic story set in the furthest reaches of our planet, in a stark desert landscape where humanity is broken, and most everyone is crazed fighting for the necessities of life. Within this world exist two rebels on the run who just might be able to restore order.",
    name: "Mad Max: Fury Road",
    ratings: 7.6,
    releaseDate: "2015-05-13"
  }
];
