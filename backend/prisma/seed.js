import { PrismaClient } from "@prisma/client";
import { encryptData } from "../src/utils/hash.js";
import { initialMovies } from "./initialMovies.js";

const prisma = new PrismaClient();
const imgUrl = "https://image.tmdb.org/t/p/w200";

async function main() {
  // ============= create movies ===============
  await prisma.movies.createMany({
    data: initialMovies.map((movie) => ({
      ...movie,
      banner: `${imgUrl}${movie.banner}`,
      releaseDate: new Date(movie.releaseDate),
    })),
  });

  // ============= create admin ===============
  const adminToCreate = {
    email: "user@gmail.com",
    password: "12345678",
  };

  const { hash, salt } = await encryptData(
    adminToCreate.password,
    adminToCreate.email
  );

  await prisma.user.create({
    data: {
      email: adminToCreate.email,
      type: "ADMIN",
      name: "Admin",
      password: hash,
      salt: salt,
    },
  });

  // ============= create client ===============
  const clientToCreate = {
    email: "client@gmail.com",
    password: "12345678",
  };

  const { hash_, salt_ } = await encryptData(
    clientToCreate.password,
    clientToCreate.email
  );

  await prisma.user.create({
    data: {
      email: clientToCreate.email,
      type: "ADMIN",
      name: "Admin",
      password: hash_,
      salt: salt_,
    },
  });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
