## Teste Desenvolvedor Full Stack

O teste consiste em criar uma aplicação com Backend(Laravel || NodeJS) que expõe uma API REST de um CRUD de usuários e filmes e uma aplicação web contendo uma interface(React/Next.JS) para login e acesso a dados de uma API externa.

# Back-end

Executar os seguintes comandos

• Entrar na pasta
    `cd /backend && cp .env.example .env && npm i`

• Preencha as variáveis de ambiente
    obs: SECRET é um variável aleatória para geração do JWT.

• Gerando o banco e preenchendo pré dados.
    `npx prisma generate`
    `npx prisma db push`
    `npx prisma db seed`

• Rodando o projeto
    `yarn dev`

# Front-end

• Entrar na pasta
    ` cd /frontend && cp .env.example .env && npm i`

• Rodando o projeto
    `yarn dev`

Faça login com:

    admin:
        email: user@gmail.com,
        password: 12345678

    cliente:
        email: client@gmail.com,
        password: 12345678
